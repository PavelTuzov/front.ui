/** библиотеки */
import { FC } from 'react';
import styled from '@emotion/styled';

import { Note } from '../Typo';
import { colors } from '../../themes/default';

/**
 * Props компонента Tag
 */
export interface TagProps {
  /**
   * Цвет текста
   * @default black
   */
  color?: colors;
  /**
   * Цвет рамочки
   */
  colorTag: colors;
  /**
   * Флаг заливки
   * @default false
   */
  background?: boolean;
}

/**
 * Styled-компонент. Стилизованная версия Tag
 */
export const TagStyled = styled.div<TagProps>`
  ${({ colorTag, background }: TagProps) => `
   display: inline-flex;
   border: ${colorTag} 1px solid;
   background: ${background ? colorTag : 'none'};
   border-radius: 3px;
   padding: 2px 6px;
   box-sizing: border-box;
 `}
`;

export const Tag: FC<TagProps> = ({ children, ...props }) => (
  <TagStyled {...props} data-test-tag="">
    <Note color={props.color} isBold>
      {children}
    </Note>
  </TagStyled>
);

export default Tag;
