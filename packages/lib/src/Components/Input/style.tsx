import styled from '@emotion/styled';
import {InputSizeTypes} from "./constants";

export const Wrapper = styled.span<{
  disabled?: boolean;
  width?: string
}>`
  ${({ disabled = false, width }) => {
    return `
      width: ${width ?? '100%'};
      position: relative;
      margin: 0;
      display: block;

      .iconControl {
        position: absolute;
        right: 0;
        padding-right: 15px;
        display: flex;
        cursor: ${disabled ? 'default' : 'pointer'};
        transition: all .1s ease;
      }
    `;
  }}
`;


export const Label = styled.label`
  ${({ theme }: any) => `
    display: block;
    width: 100%;
    & > span {
      display: block;
      color: ${theme.colors.lightBlack};
    }
  `}
`;

export const InputWrapper = styled.div`
  ${({ theme }: any) => `
    position: relative;
    display: flex;
    align-items: center;
    justify-content: space-between;
    height: 100%;
`}
`;

export const BaseInputInputWrapper = styled.div`
  ${({ theme }: any) => `
    position: relative;
    box-sizing: border-box;
    height: 54px;
    width: 100%;
    background-color: ${theme.colors.white};
  `}
`;

export const ErrorText = styled.div`
  ${({ theme }: any) => {
    return `
    display: flex;
    flex-direction: row;
    position: absolute;
    bottom: -22px;
    align-items: center;
    svg {
      fill: ${theme.colors?.main};
    }
  `;
  }}
`;

export const RubWrapper = styled.div`
  right: -19px;
  position: absolute;
`;

export const StyledInput = styled.input<{
  type?: string;
  padding?: string;
  width?: string;
  error?: boolean | string;
  hasIcon?: boolean;
  styleSize?: InputSizeTypes
}>`
  ${({
    theme,
    type,
    width = '100%',
    error,
    hasIcon = false,
    styleSize,
  }: any) => {
    return `
      position: relative;
      width: ${width};
      max-width: 100%;
      height: ${theme.inputs.size[styleSize].height};
      padding: ${hasIcon ? '0 56px 0 19px' : '0 16px'};
      transition: border .1s ease;
      border: 1px solid ${error ? theme.colors.red : theme.colors.silver};
      border-radius: ${theme.inputs.borderRadius};
      background-color: ${error ? theme.colors.lightCoral : theme.colors.white};
      box-sizing: border-box;
      outline: none;
      font-size: ${theme.inputs.size[styleSize].fontSize};
      font-family: inherit;
      line-height: inherit;
      box-sizing: border-box;
      animation-name: ${error ? 'backgroundColorChange' : 'none'};
      animation-duration: .5s;
      transition: all 0.3s;
      color: ${theme.colors.lightBlack};
      
      &:hover {
        border-color: ${
          error ? `${theme.colors.main}` : `${theme.colors.shadow}`
        };
      }

      &:focus {
        border-color: ${
          error ? `${theme.colors.main}` : `${theme.colors.darkGrey}`
        };
        
        z-index: 2;
        box-shadow: ${theme.common.focus.boxShadow};
      }
      
      &:focus, &:focus-visible {
        
      }

      &.isHovered {
        border-color: ${
          error ? `${theme.colors.main}` : `${theme.colors.shadow}`
        };
      }
      
      &:disabled {
        color: ${theme.colors.gray};
        background-color: ${theme.colors.disable};
        &::placeholder {
          opacity: 0;
        }
      }

      &::placeholder {
        color: ${theme.colors.gray};
      }

      @-webkit-keyframes autofill {
        from 
          background: #fff;
        to
          background: #fff;
      }
      
      &:-webkit-autofill {
        -webkit-box-shadow: 0 0 0 30px #fff inset !important; //!important необходим для перекрытия стандартного стиля браузера
        -webkit-animation-name: autofill;
        -webkit-animation-fill-mode: both;
        transition: background-color  5000s ease-in-out 0s;
      }
      
      @keyframes backgroundColorChange {
        from { background: ${theme.colors.pink} };
        to { background: ${theme.colors.white} };
      }
    `;
  }}
`;
