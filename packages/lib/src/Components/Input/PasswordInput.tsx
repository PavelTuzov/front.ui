/** библиотеки */
import { FC, RefObject, useEffect, useState, useRef } from 'react';

/** компоненты */
import { BaseInput } from './BaseInput';

/** Стили */
import { InputWrapper } from './style';

/** икноки */
import Icon from '../Icon/Icon';
import EyeIcon from '../Icon/Icons/EyeIcon';
import EyeCloseIcon from '../Icon/Icons/EyeCloseIcon';

/** Интерфейсы */
import { PublicInputProps } from './interfaces';

/**
 * Компонент PasswordInput
 */
const PasswordInput: FC<PublicInputProps> = ({
  type,
  value,
  placeholder,
  error,
  disabled,
  onChange,
  ...props
}) => {
  const [showPassword, setShowPassword] = useState<boolean>(false);
  const inputRef: RefObject<HTMLInputElement> | null = useRef(null);

  /**
   * Переключает видимость пароля
   */
  const changePasswordVisibility = (): void => {
    setShowPassword(!showPassword);
  };

  /**
   * Установка hover на инпут
   */
  const hoverOnInput = (): void => {
    if (!inputRef || !inputRef.current) return;
    inputRef.current.classList.add('isHovered');
  };

  /**
   * Снятие hover с инпута
   */
  const hoverFromInput = (): void => {
    if (!inputRef || !inputRef.current) return;
    inputRef.current.classList.remove('isHovered');
  };

  useEffect(() => {
    props.showPassword ? setShowPassword(true) : setShowPassword(false);
  }, []);

  return (
    <InputWrapper>
      <BaseInput
        {...props}
        type={showPassword ? 'text' : 'password'}
        onChange={onChange}
        value={value}
        placeholder={!disabled ? placeholder || 'Пароль' : ''}
        disabled={disabled}
        error={error}
        forwardedRef={inputRef}
      />
      <div
        className="iconControl"
        onClick={(): void => {
          !disabled && changePasswordVisibility();
        }}
        data-test-button=""
      >
        {showPassword ? (
          <Icon
            icon={<EyeIcon />}
            disabled={disabled}
            onMouseOver={hoverOnInput}
            onMouseOut={hoverFromInput}
          />
        ) : (
          <Icon
            icon={<EyeCloseIcon />}
            disabled={disabled}
            onMouseOver={hoverOnInput}
            onMouseOut={hoverFromInput}
          />
        )}
      </div>
    </InputWrapper>
  );
};

export default PasswordInput;
