/** библиотеки */
import { FC } from 'react';

/** компоненты */
import PhoneInput from './PhoneInput';
import PasswordInput from './PasswordInput';
import MoneyInput from './MoneyInput';
import { Text } from '../Typo/Text';
import defaultTheme from '../../themes/default';

/** интерфейсы */
import { PublicInputProps } from './interfaces';

/** стилевые компоненты */
import { Wrapper, ErrorText, StyledInput, Label } from './style';
import { InputSizeTypes } from "./constants";

/**
 * Компонент Input
 */
const Input: FC<PublicInputProps> = ({
  className,
  forwardedRef,
  width,
  styleSize = InputSizeTypes.DEFAULT,
  ...others
}: PublicInputProps) => {
  let inputComponent: JSX.Element;
  switch (others.type) {
    case 'phone':
      inputComponent = (
        <PhoneInput
          forwardedRef={forwardedRef}
          width={width}
          styleSize={styleSize}
          {...others}
        />
      );
      break;
    case 'password':
      inputComponent = (
        <PasswordInput
          forwardedRef={forwardedRef}
          width={width}
          styleSize={styleSize}
          {...others}
        />
      );
      break;
    case 'money':
      inputComponent = (
        <MoneyInput
          forwardedRef={forwardedRef}
          width={width}
          styleSize={styleSize}
          {...others}
        />
      );
      break;
    default:
      inputComponent = (
        <StyledInput ref={forwardedRef} width={width} styleSize={styleSize} {...others} />
      );
      break;
  }

  return (
    <>
      <Wrapper disabled={others.disabled} className={className} width={width}>
        <Label>{others.label && (<span>{others.label}</span>)}
          {inputComponent}
        </Label>
        {others.error && typeof others.error === 'string' && others.error.length && (
          <ErrorText>
            <Text color={defaultTheme.colors.red}>{others.error}</Text>
          </ErrorText>
        )}
      </Wrapper>

    </>
  );
};

export default Input;
