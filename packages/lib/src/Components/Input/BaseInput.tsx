/** библиотеки */
import { FC } from 'react';

/** стилевые компоненты */
import { BaseInputInputWrapper, StyledInput } from './style';

/** интерфейсы */
import { PublicInputProps } from './interfaces';

/**
 * Компонент AbstractInput
 */
export const BaseInput: FC<PublicInputProps> = ({
  type,
  width,
  value,
  name,
  disabled,
  maxLength,
  minLength,
  placeholder,
  defaultValue,
  forwardedRef,
  onChangeCustomInput,
  error,
  ...others
}: PublicInputProps) => (
  <BaseInputInputWrapper>
    <StyledInput
      type={type}
      width={width}
      name={name}
      value={value}
      disabled={disabled}
      maxLength={maxLength}
      minLength={minLength}
      placeholder={placeholder}
      defaultValue={defaultValue}
      error={error}
      ref={forwardedRef}
      {...others}
      data-test-input=""
    />
  </BaseInputInputWrapper>
);
