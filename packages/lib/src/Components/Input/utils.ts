/** константы */
import { ERROR_MESSAGE } from './constants';

/**
 * Вернёт из строки маски даты отдельно(день, месяц, год) и так же точки разделения
 * @param date
 * @returns {{separatingCharactersPos: number[], day: string, month: string, year: string}}
 */
export const splitDateLine = (date: string) => {
  const separatingCharactersPos = [date.indexOf('.'), date.lastIndexOf('.')];
  return {
    separatingCharactersPos,
    day: date.slice(0, separatingCharactersPos[0]),
    month: date.slice(
      separatingCharactersPos[0] + 1,
      separatingCharactersPos[1],
    ),
    year: date.slice(separatingCharactersPos[1] + 1, date.length),
  };
};

/**
 * Получить из строки числа
 * @param value
 * @returns {string}
 */
export const getNumberFromValue = (value: string) => {
  const valueOnlyNumber = value.match(/\d+/g);
  return valueOnlyNumber ? valueOnlyNumber.join('') : '';
};

/**
 * Вернёт нормализованное(дополненное) значение строки для Даты
 * @param value - не обработанная строка даты
 * @param fullAutoPrefix - подставлять ноль перед числом даже если это 1.
 *    @default fullAutoPrefix = false
 */
export const getNormalizationValueByDate = (
  value: string,
  fullAutoPrefix?: boolean,
) => {
  const { day, month, year } = splitDateLine(value);

  const dayOnlyNumber: string = getNumberFromValue(day);
  const monthOnlyNumber: string = getNumberFromValue(month);

  const prepareDate = {
    day:
      dayOnlyNumber.length === 1 &&
      (Number(dayOnlyNumber) > 3 || fullAutoPrefix)
        ? `0${dayOnlyNumber}`
        : Number(day) > 31
          ? 31
          : day,
    month:
      monthOnlyNumber.length === 1 &&
      (Number(monthOnlyNumber) > 1 || fullAutoPrefix)
        ? `0${monthOnlyNumber}`
        : Number(month) > 12
          ? 12
          : month,
  };

  return `${prepareDate.day}.${prepareDate.month}.${year}`;
};

/**
 * Возвращает валидные номерные части строки даты.
 * @param value - не обработанная строка даты.
 */
export const getDateValidPartNumbers = (value: string) => {
  const { day, month, year } = splitDateLine(value);

  const dayString = getNumberFromValue(day);
  const monthString = getNumberFromValue(month);
  const yearString = getNumberFromValue(year);

  const dayNumber = Number(dayString);
  const monthNumber = Number(monthString);
  const yearNumber = Number(yearString);

  const prepareDate = {
    day:
      dayString.length === 1
        ? dayNumber >= 0 && dayNumber <= 3
          ? dayNumber
          : 'uncorrect'
        : dayNumber >= 1 && dayNumber <= 31
          ? dayNumber
          : null,
    month:
      monthString.length === 1
        ? monthNumber >= 0 && monthNumber <= 1
          ? monthNumber
          : 'uncorrect'
        : monthNumber >= 1 && monthNumber <= 12
          ? monthNumber
          : null,
    year: yearNumber > 0 ? yearNumber : null,
  };

  return prepareDate;
};

/**
 * Если значение пустое, возвращает текст ошибки.
 * @param value
 */
export const checkRequired = (value: string): string => {
  if (value.length === 0) {
    return ERROR_MESSAGE.EMPTY_FIELD;
  }

  return '';
};

/**
 * Форматировать дату в строку вида dd.mm.yyyy.
 * @param date
 */
export const formatDate = (date?: Date) => {
  if (!date) return;
  const day = String(date.getDate()).padStart(2, '0');
  const month = String(date.getMonth() + 1).padStart(2, '0');
  const year = date.getFullYear();

  return [day, month, year].join('.');
};

/**
 * Преобразовать строку вида dd.mm.yyyy в дату.
 * @param str
 */
export const parseToDate = (str?: string) => {
  if (!str) return;
  const [day, month, year] = str.split('.');
  return new Date(Number(year), Number(month) - 1, Number(day));
};
