export const ERROR_MESSAGE = {
  EMPTY_FIELD: 'Поле не заполнено. Заполните поле',
  INVALID_FIELD: 'Поле заполнено неверно',
  INVALID_PHONE_FIELD: 'Телефон указан неверно',
  INVALID_DATE_FIELD: 'Дата введена неверно',
  INVALID_TIME_FIELD: 'Не указано время',
};
export const VALIDATION = {
  EMAIL_PATTERN:
    /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i,
};

/** Префикс телефонного номера по-умолчанию */
export const PHONE_DEFAULT_PREFIX = '+7';

export enum InputSizeTypes {
  LARGE = 'large',
  DEFAULT = 'default',
  SMALL = 'small',
}