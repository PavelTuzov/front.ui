/** библиотеки */
import { FC, useEffect } from 'react';

import * as React from 'react';

/** компоненты */
import { StyledInput } from './style';

/** интерфейсы */
import { PublicInputProps } from './interfaces';

/** утилиты */
import { getNumberFromValue } from './utils';

/** константы */
import { ERROR_MESSAGE, PHONE_DEFAULT_PREFIX } from './constants';

const PhoneInput: FC<PublicInputProps> = ({
  type,
  value,
  onBlur,
  onFocus,
  onChange,
  onChangeCustomInput,
  forwardedRef,
  label,
  ...others
}: PublicInputProps) => {
  /**
   * Проверяет, если поле не заполнено, возвращает ошибку.
   * @param value
   */
  const getErrorText = (value: string) => {
    const valueLength = getNumberFromValue(value).length;

    if (valueLength && valueLength < 11) {
      return ERROR_MESSAGE.INVALID_PHONE_FIELD;
    }

    return null;
  };

  /**
   * Форматирует телефон для вывод на экран +7 (999) 999-99-99
   * @param phoneNumber
   * @returns {{value: string, forSend: string, errorText: string}}
   */
  const maskPhone = (
    phoneNumber: string,
  ): { value: string; forSend: string } => {
    const phone = phoneNumber.replace('+7', '').replace(/[^0-9]/g, '');

    const phonePart = phone.match(/(\d{0,3})(\d{0,3})(\d{0,2})(\d{0,2})/) || '';
    return {
      value: `${PHONE_DEFAULT_PREFIX} ${
        !phonePart[2]
          ? phonePart[1]
          : `(${phonePart[1]}) ${phonePart[2]}${
              phonePart[3] ? ` ${phonePart[3]}` : ''
            }${phonePart[4] ? ` ${phonePart[4]}` : ''}`
      }`,
      forSend: `+7${phone.substring(0, 10)}`,
    };
  };

  /**
   * Функция, которая добавляет +7 при первом фокусе.
   * @param event
   */
  const onFocusHandler = (event: React.FocusEvent<HTMLInputElement>) => {
    if (!event.target.value && onChangeCustomInput) {
      onChangeCustomInput({
        value: `${PHONE_DEFAULT_PREFIX} `,
        forSend: PHONE_DEFAULT_PREFIX,
      });
    }

    onFocus && onFocus(event);
  };

  /**
   * Срабатывает при потере фокуса.
   * @param event
   */
  const onBlurHandler = (event: React.FocusEvent<HTMLInputElement>) => {
    let { value, forSend } = maskPhone(event.target.value);

    if (onChangeCustomInput) {
      if (forSend === PHONE_DEFAULT_PREFIX) {
        value = '';
        forSend = '';
      }
      onChangeCustomInput({
        forSend,
        value,
        errorText: getErrorText(event.target.value),
      });
    }

    onBlur && onBlur(event);
  };

  /**
   * При изменении прогоняет через маску возвращая телефон с пробелами.
   * @param event
   */
  const onChangeHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { value, forSend } = maskPhone(event.target.value);

    if (onChangeCustomInput) {
      onChangeCustomInput({
        forSend,
        value,
      });
    }

    onChange && onChange(event);
  };

  useEffect(() => {
    if (value && onChangeCustomInput) {
      const data = maskPhone(value);
      onChangeCustomInput({
        value: data.value,
        forSend: data.forSend,
      });
    }
  }, []);

  /**
   * Обработчик события вставки
   * @param event
   */
  const onPasteHandler = (
    event: React.ClipboardEvent<HTMLInputElement>,
  ): void => {
    const phone = event.clipboardData
      .getData('text/plain')
      .replace(/^(\+7|8)?\s?(\+7|7|8)/, '')
      .replace(/[^0-9]/g, '');

    const { value, forSend } = maskPhone(phone);
    if (onChangeCustomInput) {
      onChangeCustomInput({
        forSend,
        value,
      });
    }
    event.preventDefault();
  };

  return (
    <StyledInput
      value={value}
      type={type}
      onFocus={onFocusHandler}
      onBlur={onBlurHandler}
      onChange={onChangeHandler}
      onPaste={onPasteHandler}
      ref={forwardedRef}
      inputMode="numeric"
      {...others}
    />
  );
};

export default PhoneInput;
