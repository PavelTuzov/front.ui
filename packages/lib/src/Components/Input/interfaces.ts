import { RefObject } from 'react';
import {InputSizeTypes} from "./constants";

/** Типизация поля ошибки */
export type TErrorText = string | null | undefined;

/** Типизация поля "значение для отправки". */
export type TForSend = string | number | Date | Date[] | undefined;

export type onChangeCustomInputProps = {
  value: string;
  forSend?: TForSend;
  errorText?: TErrorText;
  selectDate?: Date | null;
  name?: string;
};

/** Интерфейс свойств. */
export interface PublicInputProps
  extends React.HTMLAttributes<HTMLInputElement> {
  /** Тип поля. */
  type?: string;

  /** Значение атрибута name поля ввода. */
  name?: string;

  /** Значение атрибута required поля ввода. */
  required?: boolean;

  /** Текст (значение) внутри поля ввода. */
  value?: string;

  /** Название */
  label?: string;

  /** Значение атрибута disabled поля ввода.  */
  disabled?: boolean;

  /** Максимальная длина поля. */
  maxLength?: number;

  /** Минимальная длина поля. */
  minLength?: number;

  /** Значение атрибута placeholder поля ввода. */
  placeholder?: string;

  /** Текст (значение) внутри поля ввода по умолчанию. */
  defaultValue?: string;

  /** Отображать пароль. Свойство для переключения отображения пароля при событии */
  showPassword?: boolean;

  /** Тест ошибки, если он есть то поле подсветится красным . */
  error?: string | boolean;

  /** Ширина. */
  width?: string;

  /**
   * Дает ссылку на элемент.
   * @param element
   */
  forwardedRef?: RefObject<HTMLInputElement>;

  /** Размер поля */
  styleSize?: InputSizeTypes;

  /** Флаг наличия в поле иконки. */
  hasIcon?: boolean;

  /**
   * Срабатывает при изменении значения в не стандартных поля ввода
   * таких как Phone, Money, Time и Date type input.
   */
  onChangeCustomInput?: (params: onChangeCustomInputProps) => void;

  /**
   * Событие изменения.
   * @param event
   */
  onChange?: (event: React.ChangeEvent<HTMLInputElement>) => void;

  /**
   * Событие фокуса.
   * @param event
   */
  onFocus?: (event: React.FocusEvent<HTMLInputElement>) => void;

  /**
   * Событие снятия фокуса.
   * @param event
   */
  onBlur?: (event: React.FocusEvent<HTMLInputElement>) => void;
}
