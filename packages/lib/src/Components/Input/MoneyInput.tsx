/** библиотеки */
import React, { Component } from 'react';

/** компоненты */
import { BaseInput } from './BaseInput';

/** Стили */
import { InputWrapper, RubWrapper } from './style';

/** Интерфейсы */
import { PublicInputProps } from './interfaces';

/**
 * Компонент MoneyInput
 */
export default class MoneyInput extends Component<PublicInputProps> {
  private readonly input: React.RefObject<HTMLInputElement> = React.createRef();

  private cursor = 0;

  constructor(props: PublicInputProps) {
    super(props);

    const { value, onChangeCustomInput } = props;

    if (value && onChangeCustomInput) {
      const data = this.getNormalizeNumber(value);
      onChangeCustomInput({
        value: data.value,
        forSend: data.forSend,
      });
    }
  }

  componentDidUpdate() {
    this.setSelection();
  }

  setCursorPositions = () => {
    if (this.input.current) {
      this.input.current.selectionStart = this.cursor;
      this.input.current.selectionEnd = this.cursor;
    }
  };

  /**
   * Получает отформатированное значение input
   * @param notProcessedValue неотформатированное значение
   * @return {object} Объект со значениями для отправки на сервер и для отображения
   */
  getNormalizeNumber = (notProcessedValue: string | number) => {
    const value = String(notProcessedValue);
    const searchedComa = value.search(/\.|\,/);
    const isHasComa = searchedComa !== -1;
    const comaPosition = isHasComa ? searchedComa : value.length;
    const intPartOnlyNumber = value.slice(0, comaPosition).match(/\d+/g);
    const intPart: string = intPartOnlyNumber ? intPartOnlyNumber.join('') : '';
    const floatPart: RegExpMatchArray | null = value
      .slice(comaPosition, value.length)
      .substring(1, 3)
      .match(/\d{1,2}/g);
    let prepareIntPart = '';

    const intPartParsed = Number(intPart).toString();
    for (let count = 0, i = intPartParsed.length; i > 0; i--) {
      if (count === 3) {
        count = 0;
        prepareIntPart = ` ${prepareIntPart}`;
      }
      count += 1;
      prepareIntPart = intPartParsed[i - 1] + prepareIntPart;
    }

    return {
      value: `${prepareIntPart}${isHasComa ? `,${floatPart || ''}` : ''}`,
      forSend: Number(`${intPart}.${floatPart || ''}`),
    };
  };

  /**
   * Вычисляет текущую позицию курсора
   * @param oldValue Старое значение
   * @param newValue Новое значение
   */
  setCursor = (oldValue: string, newValue: string) => {
    if (!this.input.current || !this.input.current.selectionStart) return;
    const {
      current: { selectionStart },
    } = this.input;
    const { length: newLength } = newValue;
    const { length: oldLength } = oldValue;
    const cursorCalc = newLength - oldLength + selectionStart;
    const cursorStart = selectionStart <= newLength ? selectionStart : 0;
    this.cursor =
      cursorCalc >= 0 && cursorCalc <= newLength ? cursorCalc : cursorStart;
  };

  /**
   * Устанавливает позицию курсора в input
   */
  setSelection = () => {
    if (!this.input.current || !this.input.current.selectionStart) return;
    const { value } = this.props;
    const {
      current: { selectionStart, value: inputValue },
    } = this.input;
    let { cursor } = this;
    if (selectionStart < cursor && inputValue !== value) {
      cursor -= 1;
    }
    if (this.input.current && value) {
      this.input.current.value = value;
      this.input.current.selectionEnd = cursor;
      this.input.current.selectionStart = cursor;
    }
  };

  /**
   * При изменении прогоняет через маску возвращая число пробелами и нормальной запятой
   * @param event
   */
  onChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { value, forSend } = this.getNormalizeNumber(event.target.value);

    this.setCursor(event.target.value, value);

    const { onChangeCustomInput } = this.props;
    if (onChangeCustomInput) {
      onChangeCustomInput({
        value,
        forSend,
      });
    }
  };

  render() {
    const {
      value,
      disabled,
      placeholder,
      forwardedRef,
      onChangeCustomInput,
      ...others
    } = this.props;
    return (
      <InputWrapper>
        <BaseInput
          type="text"
          onChange={this.onChange}
          value={value}
          placeholder={placeholder || '0,00'}
          disabled={disabled}
          forwardedRef={this.input}
          {...others}
        />
        <RubWrapper>&#8381;</RubWrapper>
      </InputWrapper>
    );
  }
}
