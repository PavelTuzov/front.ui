/* Библиотеки */
import { useState, useEffect, useRef, RefObject } from 'react';

import * as React from 'react';
import { RemoveScroll } from 'react-remove-scroll';
import { CSSTransition } from 'react-transition-group';
import { useMediaQuery } from 'react-responsive';

/* Интерфейсы */
import { SidePageProps } from './SidePage.types';

/* Стилизованные компоненты */
import {
  SidePageWrapper,
  StyledHeader,
  StyledFooter,
  TransitionWrapper,
  StyledHeaderSticky,
  SidePageColumn,
} from './SidePage.styles';

/* Компоненты */
import { H3, Text } from '../Typo/index';
import Icon from '../Icon/Icon';
import CloseIcon from '../Icon/Icons/CloseMinIcon';

/* Константы */
import {
  ANIMATION_DURATION,
  END_OF_TITLE,
  STICKY_HEADER_INIT_HEIGHT,
} from './SidePage.const';

/* Хуки */
import ResizeHelper, { ResizeHelperProps } from '../../utils/ResizeHelper';

export const SidePage: React.FC<SidePageProps> = ({
  children,
  width,
  size,
  disableShowWrapper,
  show,
  footerContainer,
  isOnlyMobileFooter,
  onCloseClick,
  closeWithWrapper,
  headerText,
  removeScrollBar = true,
  sideBar,
  showSideBar = false,
  ...others
}) => {
  /* Флаг скроллинга контента */
  const [isScrolling, setIsScrolling] = useState<boolean>(false);
  /* Высота Header */
  const [headerHeight, setHeaderHeight] = useState<number>(0);
  /* Высота Header Sticky */
  const [headerStickyHeight, setHeaderStickyHeight] = useState<number>(
    STICKY_HEADER_INIT_HEIGHT,
  );
  /* Высота SidePage Content */
  const [contentHeight, setContentHeight] = useState<number>(0);
  /* Высота содержимого */
  const [containerHeight, setContainerHeight] = useState<number>(0);
  /* Высота Footer */
  const [footerHeight, setFooterHeight] = useState<number>(0);
  /* Header */
  const headerRef: RefObject<HTMLDivElement> | null = useRef(null);
  /* Header Sticky */
  const headerStickyRef: RefObject<HTMLDivElement> | null = useRef(null);
  /* SidePage Content */
  const contentRef: RefObject<HTMLDivElement> | null = useRef(null);
  /* Содержимое */
  const containerRef: RefObject<HTMLDivElement> | null = useRef(null);
  /* Подвал */
  const footerRef: RefObject<HTMLDivElement> | null = useRef(null);
  /** Вычисление ширины экрана */
  const isDesktop940 = useMediaQuery({
    query: '(min-width: 940px)',
  });

  /* Скрывает компонент при нажатии Esc */
  const handlerEscape = (event: KeyboardEvent) => {
    if (event.key === 'Escape' && show) onCloseClick(event);
  };

  /* Отслеживание нажатие на Esc */
  useEffect(() => {
    document.addEventListener('keydown', (event) => handlerEscape(event));
    return () => {
      document.removeEventListener('keydown', (event) => handlerEscape(event));
    };
  }, [show]);

  /* Устанавливает высоту Header и Title */
  const setHeight = (isResize?: boolean) => {
    if (isScrolling) {
      setHeaderStickyHeight(
        headerStickyRef?.current?.getBoundingClientRect().height || 0,
      );
    } else if (!isResize)
      setHeaderHeight(headerRef?.current?.getBoundingClientRect().height || 0);

    setContentHeight(contentRef?.current?.getBoundingClientRect().height || 0);
    setContainerHeight(
      containerRef?.current?.getBoundingClientRect().height || 0,
    );
    setFooterHeight(footerRef?.current?.getBoundingClientRect().height || 0);
  };

  /* Действия при изменении размеров окна */
  const onResize = () => {
    setHeight(true);
  };

  /* Вычисление первоначальной высоты заголовка для корректного отображения контента */
  useEffect(() => {
    if (show) {
      setHeight();
    }
  }, [show, headerHeight]);

  /* Слушатель на изменение размера. Обрабатывает ситуацию изменения высоты Header и Title пользователем */
  useEffect(() => {
    const resizeHelper: ResizeHelperProps = ResizeHelper();
    window.addEventListener('scroll', onResize);
    resizeHelper.addSubscription(onResize);
    return (): void => {
      window.removeEventListener('scroll', onResize);
      resizeHelper.removeSubscription(onResize);
    };
  }, []);

  /* Вызывается при скроллинге контента */
  const handleScroll = (event: React.UIEvent<HTMLDivElement>) => {
    // Если вдруг заголовок в процессе уже открытого SidePage динамически изменится
    setHeight();
    setIsScrolling(
      (event.target as HTMLDivElement).scrollTop >= headerHeight - END_OF_TITLE,
    );
  };

  /* Скрывает компонент при клике на затемнение */
  const onCloseWrapper = (event: React.MouseEvent) => {
    if (!closeWithWrapper || disableShowWrapper) return;
    onCloseClick(event);
  };

  /* Скрывает компонент при клике на иконку */
  const onCloseIcon = (event: React.MouseEvent) => {
    onCloseClick(event);
  };
  // Отступ для Footer
  // @todo: вынужденная мера. На некоторых моделях iPhone при обычном flex позиционировании, нельзя долистать до конца.
  const footerMargin =
    containerHeight -
    contentHeight -
    footerHeight -
    headerHeight -
    headerStickyHeight;

  return (
    <RemoveScroll
      enabled={!disableShowWrapper && show}
      removeScrollBar={removeScrollBar}
    >
      <TransitionWrapper>
        <CSSTransition
          in={show}
          // css animation и timeout не совпадают точно.
          timeout={ANIMATION_DURATION}
          classNames={{
            enterActive: 'enter',
            exitActive: 'exit',
          }}
          unmountOnExit
        >
          {/*
          Не надо передавать onCloseClick, иначе получим warning в консоли
          // @ts-ignore */}
          <SidePageWrapper
            size={size}
            disableShowWrapper={disableShowWrapper}
            footerContainer={footerContainer}
            show={show}
            width={width}
            onClick={onCloseWrapper}
            headerHeight={headerHeight}
            hasSideBar={showSideBar && isDesktop940}
            {...others}
          >
            <div
              className="sidepage__container"
              onScroll={handleScroll}
              ref={containerRef}
            >
              <StyledHeader ref={headerRef} isScrolling={isScrolling} hasSideBar={showSideBar && isDesktop940} >
                {!isScrolling && (
                  <>
                    <H3 title={headerText} data-test-sidepage-header="">
                      {headerText}
                    </H3>
                    <Icon
                      icon={<CloseIcon />}
                      highlight
                      onClick={onCloseIcon}
                      role="button"
                      className={"sidepage__close"}
                    />
                  </>
                )}
              </StyledHeader>
              <StyledHeaderSticky hasSideBar={showSideBar && isDesktop940} ref={headerStickyRef}>
                {isScrolling && (
                  <>
                    <Text
                      isBold
                      title={headerText}
                      data-test-sidepage-header=""
                    >
                      {headerText}
                    </Text>
                    <Icon
                      icon={<CloseIcon />}
                      highlight
                      onClick={onCloseIcon}
                      role="button"
                      data-test-button=""
                    />
                  </>
                )}
              </StyledHeaderSticky>
              {sideBar && isDesktop940 && (
                <SidePageColumn hasSideBar={showSideBar} headerHeight={headerHeight}>
                  {sideBar}
                </SidePageColumn>
              )}
              <div className="sidepage__content">
                {/* Обёртка в ещё один блок обязательна - иначе на некоторых моделях iPhone не будет пролистывать до конца */}
                <div>
                  <div ref={contentRef}>
                    {children}
                    {isOnlyMobileFooter && isDesktop940 && footerContainer && (
                      <div className="footer">{footerContainer}</div>
                    )}
                  </div>
                  {footerContainer &&
                    ((isOnlyMobileFooter && !isDesktop940) ||
                      !isOnlyMobileFooter) && (
                      <StyledFooter
                        hasFooter={Boolean(footerContainer)}
                        ref={footerRef}
                        footerMargin={footerMargin}
                      >
                        {footerContainer}
                      </StyledFooter>
                    )}
                </div>

              </div>
            </div>
          </SidePageWrapper>
        </CSSTransition>
      </TransitionWrapper>
    </RemoveScroll>
  );
};
