/* Библиотеки */
import styled from '@emotion/styled';
/* Интерфейсы */
import {
  SidePageWrapperProps,
  SidePageFooterProps,
  SidePageHeaderProps,
  SidePageSize, SidePageColumnProps,
} from './SidePage.types';
/* Константы */
import {
  ANIMATION_DURATION,
  STICKY_HEADER_INIT_HEIGHT,
} from './SidePage.const';

export const SidePageWrapper = styled.div<SidePageWrapperProps>`
  ${({
    theme: { sidePage, screenSizes },
    width,
    size,
    disableShowWrapper,
    show,
    footerContainer,
    headerHeight,
    hasSideBar
  }) => {
    const width940 =
      size === SidePageSize.SMALL
        ? screenSizes.maxWidth940
        : screenSizes.maxWidth940 * 2 + screenSizes.marginBetweenBlocks;
    const width1100 =
      size === SidePageSize.SMALL
        ? screenSizes.maxWidth1100
        : screenSizes.maxWidth1100 * 2 + screenSizes.marginBetweenBlocks;
    const width1280 =
      size === SidePageSize.SMALL
        ? screenSizes.maxWidth1280
        : screenSizes.maxWidth1280 * 2 + screenSizes.marginBetweenBlocks;
    return `
      display: block;
      position: fixed;
      top: 0;
      bottom: 0;
      right: 0;
      background: ${show ? sidePage.background : sidePage.initialBackground};
      backdrop-filter: blur(2px);
      transition: all 0.3s;
      z-index: ${sidePage.zIndex};
      width: ${disableShowWrapper ? '0px' : '100%'};
      @media(max-width: ${screenSizes.desktop940}px) {
        width: 100%;
        display: 'flex';
      }
      .sidepage__wrapper {
        position: relative;
      }
      .sidepage__close {
        & svg {
          width: 40px;
          height: 40px;
        }
      }
      .sidepage__container {
        width: ${width || `${width1280}px`};
        background: ${sidePage.container.background};
        z-index: ${sidePage.container.zIndex};
        height: 100%;
        transition: width 0.3s;
        float: right;
        padding: 0px 32px ${footerContainer ? '0px' : '32px'} 32px;
        overflow-y: auto;
        scroll-behavior: smooth;
        transform: translateX(0);
        position: relative;
        @media(min-width: ${screenSizes.desktop940}px) {
          width: ${width || `${width940}px`};
        }
        @media(min-width: ${screenSizes.desktop1100}px) {
          width: ${width || `${width1100}px`};
        }
        @media(min-width: ${screenSizes.desktop1280}px) {
          width: ${width || `${width1280}px`};
        }
        @media(max-width: ${screenSizes.desktop940}px) {
          width: 100%;
        }

      .sidepage__content {
        width: ${hasSideBar ? 60 : 100}%;
        transition: width 0.3s;
        float: left;
        display: flex;
        flex-direction: column;
        justify-content: space-between;
        margin-top: ${headerHeight}px;

        .footer {
          width: 100%;
          padding: 32px 0 32px 0;
        }
      }
    `;
  }}
`;

export const SidePageColumn = styled.div<SidePageColumnProps>`
  ${({ hasSideBar, headerHeight }) => {
    return `
      background: blue;
      margin: -${headerHeight}px -32px -32px 0;
      height: calc(100% + 46px);
      position: sticky;
      top: -${headerHeight}px;
      right: -32px;
      overflow: hidden;
      float: right;
      transition: width 0.3s;
      width: ${hasSideBar ? '40%' : '0'};
    `;
  }}
`;

/* Стилизованный компонент основного заголовка блока SidePage */
export const StyledHeader = styled.div<SidePageHeaderProps>`
  ${({ isScrolling, hasSideBar }) => {
    return `
        display: flex;
        flex-direction: row;
        // 64px - ширина иконки + padding справа
        width: calc(${hasSideBar ? 60 : 100}% - 64px);
        transition: width 0.3s;
        justify-content: space-between;
        align-items: flex-start;
        background: white;
        z-index: 999;
        position: absolute;
        top: 0;
        visibility: ${isScrolling ? 'hidden' : 'visible'};
        h3 {
          width: 85%;
          margin-top: 30px;
          margin-bottom: 0;
        }
        h3 + span {
          position: sticky;
          top: 0;
        }
        svg {
          cursor: pointer;
          margin-top: 20px;
        }
    `;
  }};
`;
/* Стилизованный компонент sticky заголовка блока SidePage */
export const StyledHeaderSticky = styled.div<{hasSideBar?: boolean}>`
  ${({ hasSideBar }) => {
    return `
      display: flex;
      flex-direction: row;
      width: ${hasSideBar ? 60 : 100}%;
      justify-content: space-between;
      align-items: center;
      position: sticky;
      top: 0;
      background: white;
      min-height: ${STICKY_HEADER_INIT_HEIGHT}px;
      z-index: 998;
      span[data-test-sidepage-header] {
        margin: 15px 0px 15px 0px;
        width: 85%;
        text-overflow: ellipsis;
        overflow: hidden;
        white-space: nowrap;
      }
    `;
  }}
`;

/* Стилизованный компонент подвала SidePage */
export const StyledFooter = styled.div<SidePageFooterProps>`
  ${({ hasFooter, footerMargin }: SidePageFooterProps) => {
    return `
      width: 100%;
      background: white;
      position: sticky;
      bottom: ${hasFooter ? 0 : -32}px;
      padding: ${hasFooter ? '32px 0 32px 0' : '0px'};
      margin-top: ${hasFooter && footerMargin > 0 ? footerMargin : 0}px;
    `;
  }}
`;

export const TransitionWrapper = styled.div`
  ${({ theme: { sidePage } }) => {
    return `
  .enter {
    animation: fadeIn ${ANIMATION_DURATION}ms ease;

    .sidepage__container {
      animation: slideIn ${ANIMATION_DURATION}ms ease;
    }
  }

  .exit {
    animation: fadeOut ${ANIMATION_DURATION}ms ease;

    .sidepage__container {
      animation: slideOut ${ANIMATION_DURATION}ms ease;
    }
  }

  @keyframes slideIn {
    0% {
      transform: translateX(100%);
    }
    100% {
      transform: translateX(0);
    }
  }

  @keyframes slideOut {
    0% {
      transform: translateX(0);
    }
    100% {
      transform: translateX(100%);
    }
  }
  
  @keyframes fadeIn {
    0% {
      background: ${sidePage.initialBackground};
    }
    100% {
      background: ${sidePage.background};
    }
  }

  @keyframes fadeOut {
    0% {
      background: ${sidePage.background};
    }
    100% {
      background: ${sidePage.initialBackground};
    }
  }
  `;
  }}
`;
