/* Время анимации (ms) */
export const ANIMATION_DURATION = 300;
/* Начальная высота Sticky Header */
export const STICKY_HEADER_INIT_HEIGHT = 51;
/* Отступ с конца заголовка, начиная с которого следует отображать текстовый Header */
export const END_OF_TITLE = 40;
