import * as React from 'react';

/* Свойства компонента SidePage */
export interface SidePageProps {
  /** Флаг отображения компонента */
  show: boolean;
  /** Обработчик нажатия на кнопку закрытия */
  onCloseClick: (e: React.MouseEvent | KeyboardEvent) => any;
  /** Флаг выключения блокирования страницы и отображения затемнения */
  disableShowWrapper?: boolean;
  /** Ширина страницы */
  width?: string;
  /** Размер компонента */
  size?: SidePageSize;
  /** Скрывать SidePage при клике на затемнение */
  closeWithWrapper?: boolean;
  /** Текст заголовка */
  headerText?: string;
  /** Элементы, которые должны отображаться в подвале */
  footerContainer?: JSX.Element;
  /** Скрыть сколлбар
   * @default true
   * */
  removeScrollBar?: boolean;
  /** footer отображается только в мобильной версии, в десктопной версии автоматически подставляется в конец основного контента */
  isOnlyMobileFooter?: boolean;
  /** Дополнительная колонка с контентом */
  sideBar?: JSX.Element
  /** Показывать или скрывать колонку с доп контентом */
  showSideBar?: boolean

}

/* Возможные варианты размеров для свойства size */
export enum SidePageSize {
  /* X1 */
  SMALL = 1,
  /* X2 */
  MEDIUM = 2,
}

/* Свойства стилизованного компонента Footer */
export interface SidePageFooterProps {
  // Флаг наличия контента
  hasFooter: boolean;
  // Отступ для Footer
  footerMargin: number;
}

/* Свойства стилизованного компонента Wrapper */
export interface SidePageWrapperProps extends SidePageProps {
  // Высота Header
  headerHeight: number;
  /** Дополнительная колонка с контентом */
  hasSideBar?: boolean
}

/* Свойства стилизованного компонента Footer */
export interface SidePageHeaderProps {
  // Флаг пролистывания контента
  isScrolling: boolean;
  /** Дополнительная колонка с контентом */
  hasSideBar?: boolean
}

export interface SidePageColumnProps {
  /** Дополнительная колонка с контентом */
  hasSideBar?: boolean
  // Высота Header
  headerHeight: number;
}