import * as React from 'react';
import { ReactNode } from 'react';
import { CheckBoxSizeTypes } from "./constants";

/**
 * Props компонента CheckBox
 */
export interface CheckboxProps extends React.HTMLProps<HTMLInputElement> {
  children?: ReactNode;
  error?: string | boolean;
  styleSize?: CheckBoxSizeTypes

}
