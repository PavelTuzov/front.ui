
/**
 * large - большой чекбокс
 * default - стандартный чекбокс
 * small - маленький чекбокс
 */
export enum CheckBoxSizeTypes {
  LARGE = 'large',
  DEFAULT = 'default',
  SMALL = 'small',
}