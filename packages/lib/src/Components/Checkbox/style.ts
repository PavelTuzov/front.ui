import styled from '@emotion/styled';

/** стилевые компоненты */
export const Wrapper = styled.span`
  display: inline-block;
`;

export const ErrorText = styled.span`
  margin-top: 5px;
`;
