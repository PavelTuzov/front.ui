/** библиотеки */
import { FC } from 'react';
import Text from '../Typo/Text';

/** стили */
import { Wrapper, ErrorText } from './style';
import { colors } from '../../themes/default';

import { LegacyRefHelper } from '../../utils/typescriptHelpers';
import { CheckboxProps } from './interfaces';
import { CheckBoxSizeTypes } from "./constants";

import styled from '@emotion/styled';

/**
 * Нестилизованная версия Checkbox.
 * @param children children элемента
 * @param isSmall размер
 * @param props остальные props
 * @constructor
 */
export const UnstyledCheckbox: FC<CheckboxProps> = ({
  className,
  children,
  error,
  styleSize = CheckBoxSizeTypes.DEFAULT,
  ...props
}: CheckboxProps) => {
  return (
    <Wrapper className={className}>
      <label>
        <input
          type="checkbox"
          disabled={props.disabled}
          {...props}
        />
        <Text>{children}</Text>
      </label>
      {error && typeof error === 'string' && error.length && (
        <ErrorText data-test-error="">
          <Text color={colors.red}>{error}</Text>
        </ErrorText>
      )}
    </Wrapper>
  );
};

/**
 * Стилизованная версия кнопки
 */
const StyledCheckbox = styled((props: any) => UnstyledCheckbox({ ...props }))`
  ${({ theme, error, styleSize }): string => {
    return `
      position: relative;
      padding-right: 1rem;
      user-select: none;
      align-items: center;
      
      label {
        position: relative;
        display: flex;
        align-items: center;
        cursor: pointer;
      }
    
      input {
        --active: ${theme.colors.main};
        --active-inner: #fff;
        --focus: ${theme.common.focus.boxShadow};
        --border: ${theme.colors.gray};
        --border-hover: ${theme.colors.main};
        --background: #${theme.colors.white};
        --disabled: ${theme.colors.disable};
        --disabled-inner: ${theme.colors.disableDark};
        -webkit-appearance: none;
        -moz-appearance: none;
        height: ${theme.checkbox[styleSize].checkboxSize};
        width: ${theme.checkbox[styleSize].checkboxSize};
        min-width: ${theme.checkbox[styleSize].checkboxSize};
        max-width: ${theme.checkbox[styleSize].checkboxSize};
        outline: none;
        display: inline-block;
        vertical-align: top;
        position: relative;
        margin: 0;
        cursor: pointer;
        border: 1px solid var(--bc, var(--border));
        background: var(--b, var(--background));
        transition: background .3s, border-color .3s, box-shadow .2s;
        border-radius: 0px;
        &:after {
          content: '';
          display: block;
          position: absolute;
          transition: transform var(--d-t, .3s) var(--d-t-e, ease), opacity var(--d-o, .2s);
          width: 5px;
          height: 9px;
          border: 2px solid var(--active-inner);
          border-top: 0;
          border-left: 0;
          left: ${theme.checkbox[styleSize].after.left};
          top: ${theme.checkbox[styleSize].after.top};
          transform: rotate(var(--r, 45deg));
        }
        
        &:checked {
          --b: var(--active);
          --bc: var(--active);
          --d-o: .3s;
          --d-t: .6s;
          --d-t-e: cubic-bezier(.2, .85, .32, 1.2);
        }

        &:disabled {
          --b: var(--disabled);
          cursor: not-allowed;
          opacity: .9;
          &:after {
            display: none;
          }
          &:checked {
            &:after {
              display: block;
            }
            --b: var(--disabled-inner);
            --bc: var(--border);
          }
          & + label {
            cursor: not-allowed;
          }
        }

        &:hover {
          &:not(:checked) {
            &:not(:disabled) {
              --bc: var(--border-hover);
            }
          }
        }
        &:focus {
          box-shadow: var(--focus);
          z-index: 2;
        }

        & + span {
          cursor: pointer;
          margin-left: 0.5rem;
          color: ${theme.colors.lightBlack};
        }
      }
     `;
  }}
`;

const Checkbox: FC<CheckboxProps> = ({
  children,
  ref,
  styleSize = CheckBoxSizeTypes.DEFAULT,
  ...props
}: CheckboxProps) => (
  <StyledCheckbox ref={ref as LegacyRefHelper<HTMLInputElement>} styleSize={styleSize} {...props}>
    {children}
  </StyledCheckbox>
);

export default Checkbox;
