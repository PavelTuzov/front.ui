import * as React from 'react';
import { ReactElement, ReactNode } from 'react';
import { DropDownDirectionTypes, DropDownSizeTypes, DropDownStyleTypes } from "./constants";

/**
 * Props компонента CheckBox
 */
export interface DropDownProps extends React.HTMLProps<HTMLButtonElement> {
  children?: ReactNode;
  styleType?: DropDownStyleTypes,
  styleSize?: DropDownSizeTypes,
  direction?: DropDownDirectionTypes,
  options: DropDownItemProps[],
  defaultOption: DropDownItemProps;
  handleChange: (value: DropDownItemProps) => void;
  minWidth?: string;
  width?: string;
  error?: string | boolean;
  disabled?: boolean;
  isLoading?: boolean
}

export interface DropDownItemProps {
  value: ReactElement | string;
  key: string;
  icon?: ReactElement;
}
