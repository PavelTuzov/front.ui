import styled from '@emotion/styled';
import { DropDownDirectionTypes } from './constants';

/** стилевые компоненты */
export const DropDownLoader = styled.span<{ styleSize: any, styleType: any }>`
  ${({ theme, styleSize }: any) => {
    return `
      width: 100%;
      text-align: center;      
      filter: grayscale(100%);
      background: #eee;

      & .ip {
        width: auto;
        height: 1em;
      }
    `;
  }}
`;
export const DropDownHeader = styled.span<{ styleSize: any, styleType: any}>`
  ${({ theme, styleSize }: any) => {
    return `
      justify-content: left;
      //white-space: nowrap;
      align-items: center;
      display: flex;
      justify-content: flex-start;
      width: 100%;
      padding: ${theme.dropDown.size[styleSize].padding};
      height:  ${theme.dropDown.size[styleSize].height};
      line-height: ${theme.dropDown.size[styleSize].fontSize};
      text-align: left;
      overflow: hidden;
      &::selection {
        background: transparent;
      }
    `
  }}
`;
export const DropDownIcon = styled('span')<{ styleSize: any }>`
  ${({ theme, styleSize }: any) => {
  return `
    padding: ${theme.dropDown.size[styleSize].paddingArrow};
    margin: ${theme.dropDown.size[styleSize].marginArrow};
    border-left: 1px solid ${theme.colors.silver};
    min-width: 22px;    
    & svg {
      transition: all 0.1s;
      fill: ${theme.colors.gray};
    }
    &.opened {
      & svg {
        transform: rotate(180deg);
      }
    }
  `
}}
`;
export const DropDownHeaderInner = styled('span')`
  ${({ theme, direction }: any) => {
    return `
      justify-content: left;
      align-items: center;
      display: flex;
      flex-grow: 1;
      color: ${theme.colors.lightBlack};
      
      &:hover {
      }
      & svg {
        margin: 0 0.5rem 0 0;
      }
    `
  }}
`;

export const DropDownList = styled.ul<{ direction: any }>`
  ${({ theme, direction }: any) => {
    
    return `
      position: absolute;
      ${direction === DropDownDirectionTypes.DOWN ? 'top: 100%;' : 'bottom: 100%;'}
      width: 100%;
      z-index: 2;
      padding: 0;
      flex-wrap: wrap;
      background: #ffffff;
      box-sizing: border-box;
      box-shadow: 0 0 14px rgba(0, 0, 0, 0.45);
      ${direction === DropDownDirectionTypes.DOWN ? 'margin: 10px 0 0 0;' : 'margin: 0 0 10px 0;'}
      border: #777;
      & li  + li {
        margin: 0;
      }
    `
  }}
`;

export const DropDownListItem = styled.li<{ styleSize: any }>`
  ${({ theme, styleSize }: any) => {
    return `
    list-style: none;
    //white-space: nowrap;
    display: flex;
    padding: ${theme.dropDown.size[styleSize].padding};
    height:  ${theme.dropDown.size[styleSize].height};
    line-height: ${theme.dropDown.size[styleSize].fontSize};
    text-align: left;
    justify-content: left;
    align-items: center;
    width: 100%;
    margin: 0;
    color: ${theme.colors.lightBlack};
    border-top: 1px solid ${theme.colors.lightGrey};
    &:first-of-type {
      border-top: none;
    }
    &.focused {
      background: rgba(0, 0, 0, 0.1);
    }
    & svg {
      margin: 0 0.5rem 0 0;
    }
    `
  }}
`;

export const ErrorText = styled.div`
  margin-top: 5px;
`;
