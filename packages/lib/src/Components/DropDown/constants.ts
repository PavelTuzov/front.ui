/**
 * primary - основной стиль. Светлый фон, акцентированный текст
 * secondary - второстепенный. Закрашенный фон, светлый текст
 */
export enum DropDownStyleTypes {
  BUTTON = 'button',
  LINK = 'link',
}

/**
 * large - большая кнопка
 * default - стандартная кнопка
 * small - маленькая кнопка
 */
export enum DropDownSizeTypes {
  LARGE = 'large',
  DEFAULT = 'default',
  SMALL = 'small',
}

export enum DropDownDirectionTypes {
  UP = 'up',
  DOWN = 'down',
  AUTO = 'auto',
}
