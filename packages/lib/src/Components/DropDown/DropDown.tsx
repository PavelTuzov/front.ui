/** библиотеки */
/** @jsxImportSource @emotion/react */

import { FC, useEffect, useRef, useState } from 'react';

/** стили */
import { DropDownHeader, DropDownList, DropDownListItem, DropDownHeaderInner, DropDownIcon, DropDownLoader } from './style';

import { DropDownItemProps, DropDownProps } from './interfaces';
import styled from '@emotion/styled';
import { LegacyRefHelper } from "../../utils/typescriptHelpers";
import DownArrowIcon from "../Icon/Icons/DownArrowIcon";
import OutsideClickHelper, { IOutsideClickHelper } from "../../utils/OutsideClickHelper";
import { DropDownSizeTypes, DropDownStyleTypes, DropDownDirectionTypes } from "./constants";
import { Label } from "../Input/style";
import { Loader } from "../Loader";

/**
 * Нестилизованная версия DropDown.
 * @constructor
 */
export const UnstyledDropDown: FC<DropDownProps> = ({
   styleType = DropDownStyleTypes.BUTTON,
   styleSize = DropDownSizeTypes.DEFAULT,
   direction = DropDownDirectionTypes.DOWN,
   isLoading = false,
   children,
   className,
   error,
   options,
   defaultOption,
   handleChange,
   minWidth,
   disabled,
   width,
   ...props
 }: DropDownProps) => {

  const childNode = useRef<HTMLButtonElement>(null);

  const [isOpen, setIsOpen] = useState(false);
  const [selectedOption, setSelectedOption] = useState<DropDownItemProps>(defaultOption);
  const [focus, setFocus] = useState<number | undefined>()
  const toggling = () => !disabled && setIsOpen(!isOpen);
  const close = () => setIsOpen(false);

  const onOptionClicked = (item: DropDownItemProps): void => {
    if (item !== selectedOption) {
      handleChange(item);
      setSelectedOption(item);
    }
    setIsOpen(false);
  };

  /** Инстанс outsideClickHandler */
  const outsideClickHelperInstance: IOutsideClickHelper = OutsideClickHelper();

  useEffect(() => {
    setSelectedOption(defaultOption);
  }, [defaultOption]);

  useEffect(() => {
    if (childNode) {
      outsideClickHelperInstance.addSubscription(childNode, close);
    }
    return () => {
      outsideClickHelperInstance.removeSubscription(close);
    };
  }, []);

  useEffect(() => {
    if (!isOpen) {
      setFocus(undefined);
    }

    if (isOpen && selectedOption) {
      const index = options.findIndex(item => item.key === selectedOption.key);
      setFocus(index)
    }

  }, [isOpen, selectedOption]);

  const handleKeyDown = (e:any) => {
    if (e.key === 'Enter' && !disabled) {
      if (isOpen && focus !== undefined && options[focus] !== selectedOption) {
        setSelectedOption(options[focus]);
        handleChange(options[focus]);
      }

      toggling();
    }

    if (isOpen) {
      if (e.key === 'ArrowDown') {
        if (focus === undefined) {
          setFocus(0);
        }

        if (focus !== undefined && focus < options.length-1 ) {
          setFocus(focus + 1);
        }

        e.preventDefault();
      }

      if (e.key === 'ArrowUp') {
        if (focus === undefined) {
          setFocus(options.length-1);
        }

        if (focus !== undefined && focus > 0) {
          setFocus(focus - 1);
        }

        e.preventDefault();
      }
    }
  };

  return (
    <Label>
      {props.label && (<span>{props.label}</span>)}
      <button className={className} onKeyDown={handleKeyDown} ref={childNode} type={'button'} onBlur={() => setIsOpen(false)} disabled={isLoading}>
        <DropDownHeader styleSize={styleSize} styleType={styleType} onClick={toggling}>
          {isLoading ? (
            <DropDownLoader styleType={styleType} styleSize={styleSize}>
              <Loader />
            </DropDownLoader>
          ) : (
            <>
              <DropDownHeaderInner>
                {selectedOption.icon ?? selectedOption.icon}
                {selectedOption.value ?? selectedOption.value}
              </DropDownHeaderInner>
              <DropDownIcon styleSize={styleSize} className={isOpen ? 'opened' : 'closed'}>
                <DownArrowIcon />
              </DropDownIcon>
            </>
          )}
        </DropDownHeader>
        {isOpen && (
          <DropDownList direction={direction}>
            {options.map((option, index) => (
              <DropDownListItem
                styleSize={styleSize}
                onClick={() => onOptionClicked(option)} key={option.key}
                onMouseOver={() => setFocus(index)}
                className={focus === index ? "focused" : ''}
              >
                {option.icon}{option.value}
              </DropDownListItem>
            ))}
          </DropDownList>
        )}
      </button>
    </Label>
  );
};

/**
 * Стилизованная версия кнопки
 */
const StyledDropDown = styled(UnstyledDropDown)<DropDownProps>`
  ${({
       theme,
       error,
       minWidth,
       styleType = DropDownStyleTypes.BUTTON,
       styleSize = DropDownSizeTypes.DEFAULT,
       direction = DropDownDirectionTypes.DOWN,
       isLoading = false,
       disabled,
       width,
      ...props
  }: any) => {
    return `
      outline: 1px solid transparent;
      height: ${theme.dropDown.size[styleSize].height};
      box-sizing: border-box;
      font-size: ${theme.dropDown.size[styleSize].fontSize};
      border: ${styleType === DropDownStyleTypes.BUTTON ? theme.dropDown.border : 'none'};
      border-radius: ${theme.dropDown.borderRadius};
      display: inline-flex;
      justify-content: center;
      align-items: center;
      min-width: ${minWidth || theme.dropDown.minWidth};
      max-width: 100%;
      width: ${width ?? '100%'};
      background: ${styleType === DropDownStyleTypes.BUTTON ? isLoading ? '#eee' : '#fff' : 'transparent'};
      position: relative;
      opacity: ${isLoading ? 0.7 : 1};
      transition: ${theme.transitions.defaultTransition};
      ${disabled && 'color: ' + theme.colors.gray + ';'}
      ${disabled && 'opacity: 0.6;'}
      color: ${theme.colors.lightBlack};
      &:hover {
        cursor: ${isLoading ? 'default' : 'pointer'};
      }
      &:focus {
        z-index: 2;
        box-shadow: ${isLoading ? 'none' : theme.common.focus.boxShadow};
      }
      `;
    }
  }
`;

const DropDown: FC<DropDownProps> = ({
   children,
   ref,
   ...props
}: DropDownProps) => (
  <StyledDropDown ref={ref as LegacyRefHelper<HTMLButtonElement>} {...props}>
    {children}
  </StyledDropDown>
);

export default DropDown;
