import { FC, PropsWithChildren } from 'react';
import { InputGroupProps } from './interfaces';
import { StyleGroup } from "./styles";

const InputGroup: FC<PropsWithChildren<InputGroupProps>> = ({ children, tmp}) => {
  return (
    <StyleGroup>
      {children}
    </StyleGroup>
  );
};

export default InputGroup;