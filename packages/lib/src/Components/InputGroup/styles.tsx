import styled from "@emotion/styled";

export const StyleGroup = styled.div`
  display: flex;
  max-width: 100%;
  padding: 0;
  margin: 0 0 1rem 0;
  width: 100%;

  & > & {
    display: block;
    
    flex: 6;
    flex-shrink: 0;
    flex-grow: 1;
    margin-right: 0.5rem;
    margin-bottom: 0;
    width: 100%;
    & > span {
      width: 100%;
    }
    & input[type] {
      width: 100%;
    }
  }
  
  &:last-of-type {
    margin-right: 0;
  }
  
`;