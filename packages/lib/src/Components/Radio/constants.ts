
/**
 * large - большая радио кнопка
 * default - стандартная радио кнопка
 * small - маленькая радио кнопка
 */
export enum RadioSizeTypes {
  LARGE = 'large',
  DEFAULT = 'default',
  SMALL = 'small',
}