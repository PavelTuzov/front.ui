import * as React from 'react';
import { ReactNode } from 'react';
import { RadioSizeTypes } from "./constants";

/**
 * Props компонента Radio
 */
export interface RadioProps extends React.HTMLProps<HTMLInputElement> {
  /** Название класса */
  className?: string;
  /** Children элемента */
  children?: ReactNode;
  /** Размер элемента */
  styleSize?: RadioSizeTypes
  /** Ошибка */
  error?: string | boolean;
}
