/** библиотеки */
import { FC } from 'react';
import { Wrapper } from './style';

/** утилиты */
import { LegacyRefHelper } from '../../utils/typescriptHelpers';
import { RadioProps } from './interfaces';
import { ErrorText } from '../Checkbox/style';
import Text from '../Typo/Text';
import { colors } from '../../themes/default';
import styled from '@emotion/styled';
import {RadioSizeTypes} from "./constants";

/**
 * Нестилизованная версия Radio. Необходима для обертки в styled-компонент
 * @param className название класс
 * @param children children компонента
 * @param error
 * @param styleSize
 * @param props
 * @constructor
 */
export const UnstyledRadio = ({
  className,
  children,
  error,
  styleSize = RadioSizeTypes.DEFAULT,
  ...props
}: RadioProps) => (
  <Wrapper>
    <label className={className}>
      <input
        type="radio"
        disabled={props.disabled}
        defaultChecked={props.checked}
        name={props.name}
      />
      <Text>{children}</Text>
    </label>
    {error && typeof error === 'string' && error.length && (
      <ErrorText data-test-error="">
        <Text color={colors.red}>{error}</Text>
      </ErrorText>
    )}
  </Wrapper>
);

/**
 * Styled-компонент. Стилизованная версия Radio
 */
export const StyledRadio = styled((props: any) => UnstyledRadio({...props}))`
  ${({ theme, error, styleSize }): string => {
    return `
      position: relative;
      display: flex;
      padding-right: 1rem;
      user-select: none;
      align-items: center;
      
      label {
        position: relative;
        display: flex;
        align-items: center;
        cursor: pointer;
        height: ${theme.radio[styleSize].radioSize};
        align-items: center;
      }
    
      input {
        --active: ${theme.colors.main};
        --active-inner: #fff;
        --focus: ${theme.common.focus.boxShadow};
        --border: ${theme.colors.gray};
        --border-hover: ${theme.colors.main};
        --background: #${theme.colors.white};
        --disabled: ${theme.colors.disable};
        --disabled-inner: ${theme.colors.disableDark};
        -webkit-appearance: none;
        -moz-appearance: none;
        height: ${theme.radio[styleSize].radioSize};
        width: ${theme.radio[styleSize].radioSize};
        min-width: ${theme.radio[styleSize].radioSize};
        max-width: ${theme.radio[styleSize].radioSize};
        outline: none;
        display: inline-block;
        vertical-align: top;
        position: relative;
        margin: 0;
        cursor: pointer;
        border: 1px solid var(--bc, var(--border));
        background: var(--b, var(--background));
        transition: background .3s, border-color .3s, box-shadow .2s;
        border-radius: 50%;

        &:after {
          content: '';
          display: block;
          position: absolute;
          transition: transform var(--d-t, .3s) var(--d-t-e, ease), opacity var(--d-o, .2s);
          
          border-radius: 50%;
          background: var(--active-inner);
          opacity: 0;
          transform: scale(var(--s, .7));
          opacity: var(--o, 0);
          left: ${theme.radio[styleSize].after.left};
          top: ${theme.radio[styleSize].after.top};
          width: ${theme.radio[styleSize].after.width};
          height: ${theme.radio[styleSize].after.height};
        }
        
        &:checked {
          --b: var(--active);
          --bc: var(--active);
          --d-o: .3s;
          --d-t: .6s;
          --d-t-e: cubic-bezier(.2, .85, .32, 1.2);
          --s: .5;
          --o: 1;
        }

        &:disabled {
          --b: var(--disabled);
          cursor: not-allowed;
          opacity: .9;
          &:after {
            display: none;
          }
          &:checked {
            &:after {
              display: block;
            }
            --b: var(--disabled-inner);
            --bc: var(--border);
          }
          & + label {
            cursor: not-allowed;
          }
        }

        &:hover {
          &:not(:checked) {
            &:not(:disabled) {
              --bc: var(--border-hover);
            }
          }
        }
        &:focus {
          box-shadow: var(--focus);
          z-index: 2;
        }

        & + span {
          cursor: pointer;
          margin-left: 0.5rem;
          color: ${theme.colors.lightBlack};
        }
    `;
  }}
`;

const Radio: FC<RadioProps> = ({ children, ref, styleSize= RadioSizeTypes.DEFAULT, ...props }) => (
  <StyledRadio ref={ref as LegacyRefHelper<HTMLInputElement>} styleSize={styleSize} {...props}>
    {children}
  </StyledRadio>
);

export default Radio;
