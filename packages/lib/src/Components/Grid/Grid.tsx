import { FC } from 'react';
import { StyledContainer, StyledRow } from "./styles";
import { RowProps, ContainerProps } from "./interfaces";

export const Container: FC<ContainerProps> = ({ children, gap }: ContainerProps) => {
  return (
    <StyledContainer gap={gap}>
      {children}
    </StyledContainer>
  );
};

export const Row: FC<RowProps> = ({ children, col }: RowProps) => {
  return (
    <StyledRow col={col}>
      {children}
    </StyledRow>
  );
};
