import styled from "@emotion/styled";
import { screenSizes } from "../../themes/default";

export const StyledContainer = styled.div<{ gap: number }>`
  ${({ gap = 0 }) => {
    return `
      display: flex;
      gap: ${gap}rem;

      @media (max-width: ${screenSizes.desktop940}px) {
        display: block;
        margin: 0 0 ${gap}rem;
      }
    `
  }}
`;

export const StyledRow = styled.div<{col: number}>`
  ${({ col }) => {
    return `
      display: block;
      flex: ${col};
    `
  }}
`;