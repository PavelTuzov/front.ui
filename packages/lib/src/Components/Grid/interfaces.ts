import { ReactNode } from "react";

export interface RowProps {
  children: ReactNode,
  col: number
}

export interface ContainerProps {
  children: ReactNode,
  gap: number
}