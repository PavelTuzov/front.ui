import styled from '@emotion/styled';
import { colors } from '../../../themes/default';
import { IconSvg } from '../interfaces';

const Svg = styled.svg`
  stroke: ${colors.blue};
`;

const CheckMinIcon: IconSvg = () => {
  return (
    <Svg width="12px" height="10px" viewBox="0 0 12 10">
      <g
        transform="translate(-741.000000, -148.000000)"
        strokeWidth="2"
        fill="none"
      >
        <polyline points="742 152.049757 745.607448 156 752 149" />
      </g>
    </Svg>
  );
};

CheckMinIcon.iconState = {
  hoveredColor: [`{ fill: ${colors.amethyst}; }`],
  highlightColor: [`{ fill: ${colors.amethyst}; }`],
  disabledColor: [`{ fill: ${colors.lightGrey}; }`],
};

export default CheckMinIcon;
