import styled from '@emotion/styled';
import { colors } from '../../../themes/default';
import { IconSvg } from '../interfaces';

const Svg = styled.svg`
  fill: ${colors.gray};
`;

const FileIcons: IconSvg = () => {
  return (
    <Svg
      width="15"
      height="20"
      viewBox="0 0 15 20"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path d="M5 11H12V12H5V11Z" fill="#A0A0A0" />
      <path d="M5 14V15H12V14H5Z" fill="#A0A0A0" />
      <path d="M5 8H12V9H5V8Z" fill="#A0A0A0" />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M9.99382 0H2V18L15 17.9209V5L9.99382 0ZM9.57996 1H3V16.9939L14 16.9269V5.41458L9.57996 1Z"
        fill="#A0A0A0"
      />
      <path d="M1 2H0V20L13 19.9209V18.9297L1 19V2Z" fill="#A0A0A0" />
    </Svg>
  );
};

FileIcons.iconState = {
  hoveredColor: [`{ fill: ${colors.amethyst}; }`],
  highlightColor: [`{ fill: ${colors.amethyst}; }`],
  disabledColor: [`{ fill: ${colors.lightGrey}; }`],
};

export default FileIcons;
