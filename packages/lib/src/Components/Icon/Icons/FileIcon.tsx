import styled from '@emotion/styled';
import { colors } from '../../../themes/default';
import { IconSvg } from '../interfaces';

const Svg = styled.svg`
  fill: ${colors.gray};
`;

const FileIcon: IconSvg = () => {
  return (
    <Svg
      width="15"
      height="20"
      viewBox="0 0 15 20"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M8.82079 1H1V18.9941L14 18.9179V5.98123L8.82079 1ZM9.22363 0H0V20L15 19.9121V5.55556L9.22363 0Z"
      />
      <rect x="3" y="12" width="8" height="1" />
      <rect x="3" y="15" width="8" height="1" />
      <rect x="3" y="9" width="8" height="1" />
    </Svg>
  );
};

FileIcon.iconState = {
  hoveredColor: [`{ fill: ${colors.amethyst}; }`],
  highlightColor: [`{ fill: ${colors.amethyst}; }`],
  disabledColor: [`{ fill: ${colors.lightGrey}; }`],
};

export default FileIcon;
