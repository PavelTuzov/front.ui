import styled from '@emotion/styled';
import { colors } from '../../../themes/default';
import { IconSvg } from '../interfaces';

const Svg = styled.svg`
  stroke: ${colors.white};
`;

const CheckIcon: IconSvg = () => {
  return (
    <Svg width="14" height="11" viewBox="0 0 14 11" fill="none">
      <path
        d="M1.66649 4.99997L5.66652 8.99997L12.9998 1.66663"
        strokeWidth="2"
      />
    </Svg>
  );
};

CheckIcon.iconState = {
  disabledColor: [`{ stroke: ${colors.white}; }`],
};

export default CheckIcon;
