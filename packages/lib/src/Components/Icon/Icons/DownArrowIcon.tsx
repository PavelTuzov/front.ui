import styled from '@emotion/styled';
import { colors } from '../../../themes/default';
import { IconSvg } from '../interfaces';

const Svg = styled.svg`
  stroke: ${colors.white};
`;

const DownArrowIcon: IconSvg = () => {
  return (
    <Svg width="12" height="12" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
	 viewBox="0 0 407.437 407.437">
      <polygon points="386.258,91.567 203.718,273.512 21.179,91.567 0,112.815 203.718,315.87 407.437,112.815 "/>
    </Svg>
  );
};

DownArrowIcon.iconState = {
  disabledColor: [`{ stroke: ${colors.white}; }`],
};

export default DownArrowIcon;
