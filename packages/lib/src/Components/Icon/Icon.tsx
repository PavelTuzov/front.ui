/** библиотеки */
import { FC, HTMLProps } from 'react';

import styled from '@emotion/styled';

/** интерфейсы */
import { IconState } from './interfaces';

/**
 * Props компонента Icon
 */
export interface IconProps extends HTMLProps<HTMLSpanElement> {
  /** Иконка как компонент */
  icon: JSX.Element;
  /** Позволяет взаимодействовать с иконкой (менять цвет) */
  highlight?: boolean;
  /** Иконка недоступна для взаимодействия и имеет соответсвующий цвет */
  disabled?: boolean;
}

/**
 * Props компонента span
 */
interface SpanProps extends HTMLProps<HTMLSpanElement> {
  hoveredColor?: string[] | null;
  highlightColor?: string[] | null;
  disabledColor?: string[] | null;
}

export const StyledSpan = styled.span<SpanProps>`
  ${({ hoveredColor, highlightColor, disabledColor }) => `
    ${
      hoveredColor
        ? `
      ${hoveredColor.map((styleValue) => {
        return `&:hover svg ${styleValue}`;
      })}
      &:hover { cursor: pointer; }
    `
        : ''
    }
    ${
      highlightColor
        ? highlightColor.map((styleValue) => {
            return `&:active svg ${styleValue}`;
          })
        : ''
    }
    ${
      disabledColor
        ? disabledColor.map((styleValue) => {
            return `svg ${styleValue}`;
          })
        : ''
    }
      line-height: 0;
  `}
`;

const EmptyStyledSpan = styled.span<HTMLProps<HTMLSpanElement>>`
  line-height: 0;
`;

/**
 * Компонент Icon
 */
const Icon: FC<IconProps> = ({
  icon,
  highlight,
  disabled,
  onClick,
  as,
  ...others
}: IconProps) => {
  if (icon.type.iconState) {
    const { hoveredColor, highlightColor, disabledColor }: IconState =
      icon.type.iconState;
    return (
      <StyledSpan
        hoveredColor={disabled ? null : highlight ? hoveredColor : null}
        highlightColor={disabled ? null : highlight ? highlightColor : null}
        disabledColor={disabled ? disabledColor : null}
        onClick={disabled ? undefined : onClick}
        {...others}
      >
        {icon}
      </StyledSpan>
    );
  }
  return <EmptyStyledSpan {...others}>{icon}</EmptyStyledSpan>;
};

export default Icon;
