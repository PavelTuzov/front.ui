import { FC } from 'react';
/**
 * Интерфейс для функции, позволяющий хранить предустановленные стили
 * для выбранного состояния
 */
export interface IconSvg extends FC {
  iconState: IconState;
}

/**
 * Интерфейс для хранения предустановленных стилей
 */
export interface IconState {
  /** стили при наведении */
  hoveredColor?: string[];
  /** стили при нажатии */
  highlightColor?: string[];
  /** стили при состоянии недоступен */
  disabledColor?: string[];
}
