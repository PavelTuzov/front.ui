/** Библиотеки */
import { FC } from 'react';

import * as React from 'react';
import styled from '@emotion/styled';

/** Компоненты */
import { colors } from '../../themes/default';

/**
 * Props компонента Snoska
 */
interface Props extends React.HTMLProps<HTMLSpanElement> {
    /**
     * Цвет текста
     * @default gray
     */
    color?: colors;
    /** Толщина шрифта жирный
     * @default false
     */
    isBold?: boolean;
    /** Высота строки
     * @default 16px
     */
    lineHeight?: string;
}

/**
 * Styled-компонент. Стилизованная версия Note
 */
const Styled = styled.span<Props>`
  ${({ theme, lineHeight, color, isBold }: any) => `      
    display: inline-block;
    font-size: ${theme.typo.note.fontSize};
    font-family: ${theme.typo.note.fontFamily};
    font-weight: ${isBold ? '700' : theme.typo.note.fontWeight};
    line-height: ${lineHeight || theme.typo.note.lineHeight};
    color: ${color || theme.typo.note.color};

    a {
      color: inherit;
      font-size: ${theme.typo.note.fontSize};
      line-height: ${lineHeight || theme.typo.note.lineHeight};
      text-decoration: underline;
      &:hover {
        text-decoration: none;
      }
    }
  `}
`;

export const Note: FC<Props> = ({ children, as, ...others }) => (
    <Styled {...others}>{children}</Styled>
);

export default Note;
