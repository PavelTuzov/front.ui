/** Библиотеки */
import { FC } from 'react';

import * as React from 'react';
import styled from '@emotion/styled';

/** Компоненты */
import { colors } from '../../themes/default';

/**
 * Props компонента H2
 */
interface H2Props extends React.HTMLProps<HTMLHeadingElement> {
  /**
   * Цвет текста
   * @default black
   */
  color?: colors;
}

/**
 * Styled-компонент. Стилизованная версия H2
 */
const StyledH2 = styled.h2<H2Props>`
  ${({ theme, color }: any) => `
    margin-top: ${theme.typo.h2.marginTop};
    margin-bottom: ${theme.typo.h2.marginBottom};            
    font-family: ${theme.typo.h2.fontFamily};
    font-size: ${theme.typo.h2.fontSize};
    font-weight: ${theme.typo.h2.fontWeight};
    line-height: ${theme.typo.h2.lineHeight};
    color: ${color || theme.typo.color};
  `}
`;

export const H2: FC<H2Props> = ({ children, as, ...others }) => (
  <StyledH2 {...others}>{children}</StyledH2>
);

export default H2;
