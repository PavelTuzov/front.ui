/** Библиотеки */
import { FC } from 'react';

import * as React from 'react';
import styled from '@emotion/styled';

/** Компоненты */
import { colors } from '../../themes/default';

/**
 * Props компонента H3
 */
interface H3Props extends React.HTMLProps<HTMLHeadingElement> {
  /**
   * Цвет текста
   * @default black
   */
  color?: colors;
}

/**
 * Styled-компонент. Стилизованная версия H3
 */
const StyledH3 = styled.h3<H3Props>`
  ${({ theme, color }: any) => `
    margin-top: ${theme.typo.h3.marginTop};
    margin-bottom: ${theme.typo.h3.marginBottom};      
    font-family: ${theme.typo.h3.fontFamily};
    font-size: ${theme.typo.h3.fontSize};
    font-weight: ${theme.typo.h3.fontWeight};
    line-height: ${theme.typo.h3.lineHeight};
    color: ${color || theme.typo.color};
  `}
`;

export const H3: FC<H3Props> = ({ children, as, ...others }) => (
  <StyledH3 {...others}>{children}</StyledH3>
);

export default H3;
