/** Библиотеки */
import { FC } from 'react';

import * as React from 'react';
import styled from '@emotion/styled';

/** Компоненты */
import { colors } from '../../themes/default';

/**
 * Props компонента H1
 */
interface H1Props extends React.HTMLProps<HTMLHeadingElement> {
  /**
   * Цвет текста
   * @default black
   */
  color?: colors;
}

/**
 * Styled-компонент. Стилизованная версия H1
 */
const StyledH1 = styled.h1<H1Props>`
  ${({ theme, color }: any) => `
    margin-top: ${theme.typo.h1.marginTop};
    margin-bottom: ${theme.typo.h1.marginBottom};       
    font-family: ${theme.typo.h1.fontFamily};
    font-size: ${theme.typo.h1.fontSize};
    font-weight: ${theme.typo.h1.fontWeight};
    line-height: ${theme.typo.h1.lineHeight};
    color: ${color || theme.typo.color};
  `}
`;

export const H1: FC<H1Props> = ({ children, as, ...others }) => (
  <StyledH1 {...others}>{children}</StyledH1>
);

export default H1;
