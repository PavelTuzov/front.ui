/** Библиотеки */
import React, { FC, RefObject } from 'react';
import styled from '@emotion/styled';

/** Компоненты */
import { colors } from '../../themes/default';

/**
 * Props компонента Text2
 */
interface TextProps extends React.HTMLProps<HTMLSpanElement> {
  /**
   * Цвет текста
   * @default black
   */
  color?: colors;
  /** Толщина шрифта жирный
   * @default false
   */
  isBold?: boolean;
  /** Высота строки
   * @default 21px
   */
  lineHeight?: string;
  /**
   * Дает ссылку на элемент.
   * @param element
   */
  forwardedRef?: RefObject<HTMLSpanElement>;
}

/**
 * Styled-компонент. Стилизованная версия Text2
 */
const StyledText = styled.span<TextProps>`
  ${({ theme, lineHeight, color, isBold }) => `      
    font-family: ${theme.typo.text.fontFamily};
    font-size: ${theme.typo.text.fontSize};
    color: ${color || theme.typo.color};
    line-height: ${lineHeight || theme.typo.text.lineHeight};
    font-weight: ${isBold ? 'bold' : theme.typo.text.fontWeight};
  `}
`;

export const Text: FC<TextProps> = ({
  children,
  as,
  forwardedRef,
  ...others
}) => (
  <StyledText ref={forwardedRef} {...others}>
    {children}
  </StyledText>
);

export default Text;
