import { ReactNode } from "react";
import { UsedLocales } from "../../types";

export type SliderProps = {
  slides?: ReactNode[]
  height?: number
  lang?: UsedLocales
  autoPlay?: number | boolean
}
