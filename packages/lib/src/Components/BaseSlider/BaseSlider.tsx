import {FC, useState} from 'react';

import { SliderProps } from "./interfaces";
import { nanoid } from "nanoid";
import { StyledSwiper, StyledSwiperSlide } from "./style";
import { Navigation, Pagination, Autoplay } from "swiper";
import DownArrowIcon from "../Icon/Icons/DownArrowIcon";
import { UsedLocales } from "../../types";

const BaseSlider: FC<SliderProps> = ({
    slides,
    height = 200,
    lang = UsedLocales.ru,
    autoPlay = 4000
  }: SliderProps) => {

  const [prevEl, setPrevEl] = useState<HTMLElement | null>(null)
  const [nextEl, setNextEl] = useState<HTMLElement | null>(null)



  const pagination = {
    clickable: true,
    renderBullet: function (index: any, className: any) {
      return '<span class="' + className + '">' + (index + 1) + "</span>";
    },
  };

  const navigation = {
    prevEl,
    nextEl,
  };

console.log();

  return (
    <StyledSwiper
      className={"swiper"}
      navigation={navigation}
      pagination={pagination}
      autoplay={!autoPlay ? false : {
        delay: +autoPlay,
        disableOnInteraction: false,
        pauseOnMouseEnter: true,
      }}
      modules={[Pagination, Navigation, Autoplay]}
      slidesPerView={1}
      spaceBetween={0}
      loop={true}
      height={height}

    >
      {slides ? slides.map(item => (
        <StyledSwiperSlide key={nanoid(5)}>
          <>{item}</>
        </StyledSwiperSlide>
      )) : null}

      <div className={"swiper-button-prev"} ref={(node) => setPrevEl(node)}>
        <DownArrowIcon />
      </div>
      <div className={"swiper-button-next"} ref={(node) => setNextEl(node)}>
        <DownArrowIcon />
      </div>


    </StyledSwiper>
  );
};

export default BaseSlider;