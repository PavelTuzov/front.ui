import BaseSlider from './BaseSlider';
import { SliderProps } from "./interfaces";

export { BaseSlider, SliderProps };