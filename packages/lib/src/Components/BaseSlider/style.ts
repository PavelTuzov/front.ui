import { Swiper, SwiperSlide } from "swiper/react";
import styled from "@emotion/styled";
import { colors } from "../../themes/default";

export const StyledSwiper = styled(Swiper)<{ height?: number }>`
  ${({ theme, height }: any) => {
  return `
    width: 100%;
    height: ${height ?? 100}px;
    margin-left: auto;
    margin-right: auto;
    position: relative;
    overflow: hidden;
    list-style: none;
    padding: 0;
    /* Fix of Webkit flickering */
    z-index: 1;
  
    .swiper-wrapper {
      position: relative;
      width: 100%;
      height: 100%;
      z-index: 1;
      display: flex;
      transition-property: transform;
      box-sizing: content-box;
    }
    .swiper-android .swiper-slide,
    .swiper-wrapper {
      transform: translate3d(0px, 0, 0);
    }
    .swiper-pointer-events {
      touch-action: pan-y;
      &.swiper-vertical {
        touch-action: pan-x;
      }
    }
    .swiper-slide {
      flex-shrink: 0;
      width: 100%;
      height: 100%;
      position: relative;
      transition-property: transform;
    }
  
  
    .swiper-pagination {
      position: absolute;
      text-align: center;
      transition: 300ms opacity;
      transform: translate3d(0, 0, 0);
      z-index: 10;
      &.swiper-pagination-hidden {
        opacity: 0;
      }
      .swiper-pagination-disabled > &,
      &.swiper-pagination-disabled {
        display: none !important;
      }
    }
    /* Common Styles */
    .swiper-pagination-fraction,
    .swiper-pagination-custom,
    .swiper-horizontal > .swiper-pagination-bullets,
    .swiper-pagination-bullets.swiper-pagination-horizontal {
      bottom: 10px;
      left: 0;
      width: 100%;
    }
    /* Bullets */
    .swiper-pagination-bullets-dynamic {
      overflow: hidden;
      font-size: 0;
      .swiper-pagination-bullet {
        transform: scale(0.33);
        position: relative;
      }
      .swiper-pagination-bullet-active {
        transform: scale(1);
      }
      .swiper-pagination-bullet-active-main {
        transform: scale(1);
      }
      .swiper-pagination-bullet-active-prev {
        transform: scale(0.66);
      }
      .swiper-pagination-bullet-active-prev-prev {
        transform: scale(0.33);
      }
      .swiper-pagination-bullet-active-next {
        transform: scale(0.66);
      }
      .swiper-pagination-bullet-active-next-next {
        transform: scale(0.33);
      }
    }
    .swiper-pagination-bullet {
      width: var(--swiper-pagination-bullet-width, var(--swiper-pagination-bullet-size, 18px));
      height: var(--swiper-pagination-bullet-height, var(--swiper-pagination-bullet-size, 18px));
      line-height: var(--swiper-pagination-bullet-height, var(--swiper-pagination-bullet-size, 18px));
      display: inline-block;
      border-radius: 50%;
      background: var(--swiper-pagination-bullet-inactive-color, #000);
      opacity: var(--swiper-pagination-bullet-inactive-opacity, 0.2);
      font-size: 11px;
      color: #fff;
      transition: all 0.3s;
      cursor: pointer;
      user-select: none;
  
      .swiper-pagination-clickable & span {
        cursor: pointer;
      }
  
      &:only-child {
        display: none !important;
      }
    }
    .swiper-pagination-bullet-active {
      opacity: var(--swiper-pagination-bullet-opacity, 1);
      background: ${colors.main};
      width: 24px;
      height: 24px;
      line-height: 24px;
      
    }
  
    .swiper-vertical > .swiper-pagination-bullets,
    .swiper-pagination-vertical.swiper-pagination-bullets {
      right: 10px;
      top: 50%;
      transform: translate3d(0px, -50%, 0);
      .swiper-pagination-bullet {
        margin: var(--swiper-pagination-bullet-vertical-gap, 6px) 0;
        display: block;
      }
      &.swiper-pagination-bullets-dynamic {
        top: 50%;
        transform: translateY(-50%);
        width: 8px;
        .swiper-pagination-bullet {
          display: inline-block;
          transition: 200ms transform, 200ms top;
        }
      }
    }
    .swiper-horizontal > .swiper-pagination-bullets,
    .swiper-pagination-horizontal.swiper-pagination-bullets {
      .swiper-pagination-bullet {
        margin: 0 var(--swiper-pagination-bullet-horizontal-gap, 4px);
      }
      &.swiper-pagination-bullets-dynamic {
        left: 50%;
        transform: translateX(-50%);
        white-space: nowrap;
        .swiper-pagination-bullet {
          transition: 200ms transform, 200ms left;
        }
      }
    }
    
    /* Progress */
    .swiper-pagination-progressbar {
      background: rgba(0, 0, 0, 0.25);
      position: absolute;
      .swiper-pagination-progressbar-fill {
        background: background(0, 0, 0, 0.5);
        position: absolute;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        transform: scale(0);
        transform-origin: left top;
      }
      
      .swiper-horizontal > &,
      &.swiper-pagination-horizontal,
      .swiper-vertical > &.swiper-pagination-progressbar-opposite,
      &.swiper-pagination-vertical.swiper-pagination-progressbar-opposite {
        width: 100%;
        height: 4px;
        left: 0;
        top: 0;
      }
      .swiper-vertical > &,
      &.swiper-pagination-vertical,
      .swiper-horizontal > &.swiper-pagination-progressbar-opposite,
      &.swiper-pagination-horizontal.swiper-pagination-progressbar-opposite {
        width: 4px;
        height: 100%;
        left: 0;
        top: 0;
      }
    }
    .swiper-pagination-lock {
      display: none;
    }
  
  
    .swiper-button-prev {
      & svg {
        transform: rotate(90deg);
      }
    }
  
    .swiper-button-next {
      & svg {
        transform: rotate(-90deg);
      }
    }
  
    .swiper-button-next svg, .swiper-button-prev svg {
      fill: #fff;
    }
  
    .swiper-button-prev,
    .swiper-button-next {
      position: absolute;
      top: 50%;
      width: 46px;
      height: 46px;
      transition: all 0.3s;
      background: rgba(0, 0, 0, 0.2);
      
      margin-top: calc(0px - (46px / 2));
      z-index: 10;
      cursor: pointer;
      display: flex;
      align-items: center;
      justify-content: center;
      color: var(--swiper-navigation-color, var(--swiper-theme-color));
      &:hover {
        background: rgba(0, 0, 0, 0.4);
      }
      &.swiper-button-disabled {
        opacity: 0.35;
        cursor: auto;
        pointer-events: none;
      }
      &.swiper-button-hidden {
        opacity: 0;
        cursor: auto;
        pointer-events: none;
      }
      .swiper-navigation-disabled & {
        display: none !important;
      }
      &:after {
        font-size: var(--swiper-navigation-size);
        text-transform: none !important;
        letter-spacing: 0;
        font-variant: initial;
        line-height: 1;
      }
    }
    .swiper-button-prev,
    .swiper-rtl .swiper-button-next {
      border-top-right-radius: 4px;
      border-bottom-right-radius: 4px;
      left: 0;
      right: auto;
    }
    .swiper-button-next,
    .swiper-rtl .swiper-button-prev {
      border-top-left-radius: 4px;
      border-bottom-left-radius: 4px;
      right: 0;
      left: auto;
    }
  
    .swiper-button-lock {
      display: none;
    }
  `
}}
`;

export const StyledSwiperSlide = styled(SwiperSlide)`
  ${({ theme }: any) => {
    return `
      text-align: center;
      font-size: 18px;
    
      /* Center slide text vertically */
      display: -webkit-box;
      display: -ms-flexbox;
      display: -webkit-flex;
      display: flex;
      -webkit-box-pack: center;
      -ms-flex-pack: center;
      -webkit-justify-content: center;
      justify-content: center;
      -webkit-box-align: center;
      -ms-flex-align: center;
      -webkit-align-items: center;
      align-items: center;
    `
}}`;