/**
 * primary - основной стиль. Светлый фон, акцентированный текст
 * secondary - второстепенный. Закрашенный фон, светлый текст
 */
export enum ButtonStyleTypes {
  PRIMARY = 'primary',
  SECONDARY = 'secondary',
}

/**
 * large - большая кнопка
 * default - стандартная кнопка
 * small - маленькая кнопка
 */
export enum ButtonSizeTypes {
  LARGE = 'large',
  DEFAULT = 'default',
  SMALL = 'small',
}

/** Позиционирование иконки */
export enum ButtonIconPosition {
  LEFT = 'left',
  RIGHT = 'right',
  CENTER = 'center',
}
