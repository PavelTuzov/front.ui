import { ReactNode } from 'react';
import * as React from 'react';

import {
  ButtonIconPosition,
  ButtonStyleTypes,
  ButtonSizeTypes,
} from './constants';

/**
 * Props компонента Button
 */
export interface ButtonProps extends React.HTMLProps<HTMLButtonElement> {
  children?: string;
  className?: string;
  styleType?: ButtonStyleTypes;
  styleSize?: ButtonSizeTypes;
  background?: string;
  icon?: ReactNode;
  iconPosition?: ButtonIconPosition;
  isLoading?: boolean;
  disabled?: boolean;
  minWidth?: string;
}
