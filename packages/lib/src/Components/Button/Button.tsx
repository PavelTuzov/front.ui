/** библиотеки */
import { FC } from 'react';

/** компоненты */
// import Loader from '../Loader/Loader';

/** константы */
import {
  ButtonIconPosition,
  ButtonSizeTypes,
  ButtonStyleTypes,
} from './constants';
import { LegacyRefHelper } from '../../utils/typescriptHelpers';

/** Интерфейсы */
import { ButtonProps } from './interfaces';
import styled from '@emotion/styled';


/**
 * Нестилизованная версия кнопки. Деструктуризация некоторых компонентов проводится для того, чтобы они
 * не передались в сам тег <Button>
 * @param children children
 * @param contentClassname Имя класса для добавления к контейнеру children
 * @param icon Добавляет иконку
 * @param iconPosition Позиция иконки
 * @param styleType Тип кнопки
 * @param isLoading Показать вместо текста индикатор загрузки
 * @param type Тип кнопки
 * @param props остальные props
 * @constructor
 */
export const UnstyledButton: FC<ButtonProps> = ({
    children,
    icon,
    iconPosition = ButtonIconPosition.LEFT,
    isLoading,
    styleType = ButtonStyleTypes.PRIMARY,
    type,
    styleSize = ButtonSizeTypes.DEFAULT,
    disabled,
    ...props
  }: ButtonProps) => {
  const buttonType = type as 'reset' | 'button' | 'submit' | undefined;
  const classList = [props.className];
  isLoading ? classList.push('button__loading') : null;
  return (
    <button {...props} type={buttonType} disabled={isLoading || disabled} className={classList.join(' ')}>
      {icon ? <span className="button__icon">&nbsp;</span> : <></>}
      <span className="button__content">{children}</span>
    </button>
  );
};

/**
 * Стилизованная версия кнопки
 */
const StyledButton = styled(UnstyledButton)`
  ${({
    theme,
    styleType = ButtonStyleTypes.PRIMARY,
    isLoading,
    icon,
    iconPosition = ButtonIconPosition.LEFT,
    styleSize = ButtonSizeTypes.DEFAULT,
    minWidth,
    background,
  }): string => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const themeButtonStyle: any = theme.buttons;

    const getMinWidth = (): string => {
      if (minWidth) return minWidth;
      return themeButtonStyle[styleType].minWidth;
    };
    return `
        outline: 1px solid transparent;
        box-sizing: border-box;
        display: inline-flex;
        justify-content: center;
        align-items: center;
        min-width: ${getMinWidth()};
        border: ${themeButtonStyle[styleType].border};
        border-radius: ${themeButtonStyle[styleType].borderRadius};
        background-color: ${
          background || themeButtonStyle[styleType].backgroundColor
        };
        color: ${themeButtonStyle[styleType].color};
        outline: none;
        transition: ${themeButtonStyle[styleType].transition};
        position: relative;
        height: ${themeButtonStyle.size[styleSize].height};
        padding: ${themeButtonStyle.size[styleSize].padding};
        font-size: ${themeButtonStyle.size[styleSize].fontSize};
        &:focus, &:focus-visible {
          box-shadow: ${theme.common.focus.boxShadow};
        }
        
        &:hover {
          background-color: ${
            themeButtonStyle[styleType].hover.backgroundColor
          };
          border-color: ${themeButtonStyle[styleType].hover.borderColor};
          cursor: pointer;
          
          .button__content {
            color: ${themeButtonStyle[styleType].hover.color};
          }

          span {
            display: flex;
          }
          
          svg {
            fill: ${themeButtonStyle[styleType].hover.color};
          }
        }
    
        &:active {
          background-color: ${
            themeButtonStyle[styleType].active.backgroundColor
          };
          border-color: ${themeButtonStyle[styleType].active.borderColor};
          
          .button__content {
            color: ${themeButtonStyle[styleType].active.color};
          }
          
          svg {
            fill: ${themeButtonStyle[styleType].active.color};
          }
        }
        
        &:disabled {
          background-color: ${
            themeButtonStyle[styleType].disabled.backgroundColor
          };
          border-color: ${themeButtonStyle[styleType].disabled.borderColor};
          opacity: ${themeButtonStyle[styleType].disabled.opacity};
          cursor: ${themeButtonStyle[styleType].disabled.cursor};
          
          .button__content {
            color: ${themeButtonStyle[styleType].disabled.color};
          }
          
          svg {
            fill: ${themeButtonStyle[styleType].disabled.color};
          }
        }
        
        svg {
          fill: ${themeButtonStyle[styleType].color};
          visibility: ${isLoading ? 'hidden' : 'visible'};
        }
        
        .button__content {
          margin-left: ${
            icon && iconPosition === ButtonIconPosition.LEFT ? '9px' : '0'
          };
          margin-right: ${
            icon && iconPosition === ButtonIconPosition.RIGHT ? '9px' : '0'
          };
          color: ${themeButtonStyle[styleType].color};
          visibility: ${isLoading ? 'hidden' : 'visible'};
        }
        
        &.button__loading,
        &.button__loading:after {
          height: ${themeButtonStyle.size[styleSize].height};
          width: ${themeButtonStyle.size[styleSize].height};
          border-radius: 50%;
        }
        
        &.button__loading {
          transition-property: width height padding box-shadow border-width background opacity;
          transition-duration: 0.3s;
          transition-timing-function: ease-in-out;
          
          background: transparent;
          box-shadow: 0 0 0 #006799;
          border-top: 6px solid ${theme.colors.main};
          border-right: 6px solid ${theme.colors.accent};
          border-bottom: 6px solid ${theme.colors.accent};
          border-left: 6px solid ${theme.colors.accent};
        
          transform: translateZ(0);
          animation: load8 1s infinite linear;
          animation-delay: .2s;
          
          span {
            display: none;
          }
          
          &:hover {
            background: transparent;
          }
        
        +button {
          margin: 0 10px;
        }
     
        @-webkit-keyframes load8 {
          0% {
            -webkit-transform: rotate(0deg);
            transform: rotate(0deg);
          }
          100% {
            -webkit-transform: rotate(360deg);
            transform: rotate(360deg);
          }
        }
        @keyframes load8 {
          0% {
            -webkit-transform: rotate(0deg);
            transform: rotate(0deg);
          }
          100% {
            -webkit-transform: rotate(360deg);
            transform: rotate(360deg);
          }
        }
      `;
  }}
`;

/**
 * Компонент кнопки
 * @param children
 * @param props
 * @param ref
 * @constructor
 */
const Button: FC<ButtonProps> = ({ children, ref, ...props }: ButtonProps) => (
  <StyledButton ref={ref as LegacyRefHelper<HTMLButtonElement>} {...props}>
    {children}
  </StyledButton>
);

export default Button;
