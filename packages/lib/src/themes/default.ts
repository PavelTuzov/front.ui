/**
 * Тема по умолчанию.
 * Используется в ThemeProvider
 */
export enum colors {
  /* brand colors */
  main = '#FF2E63',
  mainDark = '#be284d',
  mainLight = '#FF2E63',
  accent = '#3C37FE',
  accentDark = '#1F1ABB',
  /* ui colors */
  success = 'green',
  warning = 'linear-gradient(264deg, #fcfaf3, #f4fbfe);',
  info = '#dce6fd',
  /* common colors */
  black = '#000000',
  lightBlack = '#252424',
  white = '#FFFFFF',
  darkGrey = '#707070',
  gray = '#A0A0A0',
  doveGray = '#6d6d6d',
  lightGrey = '#D8D8D8',
  silver = '#BEBEBE',
  whiteSmoke = '#E8E8E8',
  snow = '#FAFAFA',
  stateGray = '#4C4C4C',
  red = '#CF0000',
  lightCoral = '#FFF4F4',
  blue = '#2D67DF',

  /* delete colors */
  amethyst = '#7057CD',
  lightBlue = '#F2F4F7',

  disable = '#F6F8FF',
  disableDark = '#b7bcd2',
}

export enum screenSizes {
  desktop1280 = 1280,
  desktop1100 = 1100,
  desktop940 = 940,
  // расстояние между блоками в сетке Планеты
  marginBetweenBlocks = 32,
  // ширина на определенных размерах экрана
  maxWidth1280 = 400,
  maxWidth1100 = 340,
  maxWidth940 = 280,
}

export const sizes = {
  large: {
    height: '64px',
    fontSize: '22px',
    lineHeight: '28px',
  },
  default: {
    height: '52px',
    fontSize: '18px',
    lineHeight: '24px',
  },
  small: {
    height: '36px',
    fontSize: '16px',
    lineHeight: '20px',
  },
};

export const transitions = {
  defaultTransition: 'all linear .2s',
};

export const common = {
  /* eslint quotes: [2, "single", "avoid-escape"] */
  fontFamily: "'Fira Sans', sans-serif",
  fontSize: sizes.default.fontSize,
  lineHeight: sizes.default.lineHeight,
  backgroundColor: colors.lightGrey,

  focus: {
    boxShadow: '0 0 0 4px rgba(39, 94, 254, .3);',
    outline: `2px solid ${colors.blue}`,
  }
};

export default {
  colors,
  sizes,
  screenSizes,
  transitions,
  common,
  inputs: {
    borderRadius: '4px',
    border: `1px solid ${colors.silver}`,
    size: {
      large: {
        height: sizes.large.height,
        fontSize: sizes.large.fontSize,
        padding: '0rem 1rem',
      },
      default: {
        height: sizes.default.height,
        fontSize: sizes.default.fontSize,
        padding: '0rem 0.8rem',
      },
      small: {
        height: sizes.small.height,
        fontSize: sizes.small.fontSize,
        padding: '0rem 0.5rem',
      },
    },
  },
  checkbox: {
    small: {
      checkboxSize: '16px',
      after: {
        left: '4px',
        top: '0px',
      }
    },
    default: {
      checkboxSize: '24px',
      after: {
        left: '8px',
        top: '5px',
      }
    },
    large: {
      checkboxSize: '32px',
      after: {
        left: '12px',
        top: '8px',
      }
    }
  },
  radio: {
    small: {
      radioSize: '16px',
      after: {
        left: '0px',
        top: '0px',
        width: '14px',
        height: '14px',
      }
    },
    default: {
      radioSize: '24px',
      after: {
        left: '1px',
        top: '1px',
        width: '20px',
        height: '20px',
      }
    },
    large: {
      radioSize: '32px',
      after: {
        left: '3px',
        top: '3px',
        width: '24px',
        height: '24px',
      }
    }
  },
  popup: {
    background: 'rgb(112, 112, 112, 0.7)', // darkGrey с 0.7 opacity
    zIndex: 10000,
    color: colors.black,
    container: {
      width: '390px',
      background: colors.white,
      boxShadow: '0 2px 4px 0 rgba(0,0,0,0.5);',
      paddingTop: '22px',
      paddingRight: '56px',
      paddingBottom: '30px',
      paddingLeft: '22px',
      zIndex: 11,
    },
  },
  tooltip: {
    fontFamily: 'Fira Sans',
    fontSize: '14px',
    lineHeight: '18px',
    background: colors.white,
    color: colors.darkGrey,
    width: '254px',
    maxHeight: '200px',
    boxShadow: `0 0 6px 0 ${colors.gray}`,
    padding: '16px 38px 16px 16px',
    titleColor: colors.lightBlack,
    titleFontWeight: 'bold',
    titleMarginBottom: '24px',
    zIndex: 101,
    paddingTopBottom: '16px 0',
    titlePaddingLeftRigth: '0 19px 0 16px',
  },
  typo: {
    color: colors.black,
    h1: {
      marginTop: '33px',
      marginBottom: '33px',
      /* eslint quotes: [2, "single", "avoid-escape"] */
      fontFamily: "'Fira Sans', sans-serif",
      fontSize: '50px',
      fontWeight: 'bold',
      lineHeight: '60px',
    },
    h2: {
      marginTop: '33px',
      marginBottom: '33px',
      /* eslint quotes: [2, "single", "avoid-escape"] */
      fontFamily: "'Fira Sans', sans-serif",
      fontSize: '40px',
      fontWeight: 'bold',
      lineHeight: '47px',
    },
    h3: {
      marginTop: '30px',
      marginBottom: '30px',
      /* eslint quotes: [2, "single", "avoid-escape"] */
      fontFamily: "'Fira Sans', sans-serif",
      fontSize: '30px',
      fontWeight: 'bold',
      lineHeight: '35px',
    },
    h4: {
      marginTop: '15px',
      marginBottom: '15px',
      /* eslint quotes: [2, "single", "avoid-escape"] */
      fontFamily: "'Fira Sans', sans-serif",
      fontSize: '23px',
      fontWeight: 'normal',
      lineHeight: '27px',
    },
    h5: {
      marginTop: '0',
      marginBottom: '0',
      /* eslint quotes: [2, "single", "avoid-escape"] */
      fontFamily: "'Fira Sans', sans-serif",
      fontSize: '16px',
      fontWeight: 'bold',
      lineHeight: '20px',
    },
    h6: {
      marginTop: '0',
      marginBottom: '0',
      /* eslint quotes: [2, "single", "avoid-escape"] */
      fontFamily: "'Fira Sans', sans-serif",
      fontSize: '16px',
      fontWeight: 'normal',
      lineHeight: '20px',
    },
    h7: {
      marginTop: '0',
      marginBottom: '0',
      /* eslint quotes: [2, "single", "avoid-escape"] */
      fontFamily: "'Fira Sans', sans-serif",
      fontSize: '14px',
      fontWeight: 'normal',
      lineHeight: '18px',
    },
    note: {
      fontSize: '0.6rem',
      /* eslint quotes: [2, "single", "avoid-escape"] */
      fontFamily: "'Fira Sans', sans-serif",
      lineHeight: '0.8rem',
      letterSpacing: '-0.01em',
      fontWeight: '400',
      color: colors.gray,
    },
    quote: {
      /* eslint quotes: [2, "single", "avoid-escape"] */
      fontFamily: "'Fira Sans', sans-serif",
      fontSize: '32px',
      fontWeight: 'normal',
      fontStyle: 'italic',
      lineHeight: '40px',
      textAlign: 'center',
    },
    text: {
      /* eslint quotes: [2, "single", "avoid-escape"] */
      fontFamily: "'Fira Sans', sans-serif",
      fontSize: '16px',
      lineHeight: '24px',
      fontWeight: 'normal',
    },
    textSmall: {
      /* eslint quotes: [2, "single", "avoid-escape"] */
      fontFamily: "'Fira Sans', sans-serif",
      fontSize: '14px',
      lineHeight: '20px',
      fontWeight: 'normal',
    },
    promo1: {
      marginTop: '47px',
      marginBottom: '47px',
      /* eslint quotes: [2, "single", "avoid-escape"] */
      fontFamily: "'Oswald Extra-Light Italic', sans-serif",
      fontSize: '70px',
      fontWeight: 200,
      lineHeight: '90px',
      textAlign: 'left',
    },
    promo2: {
      marginTop: '35px',
      marginBottom: '35px',
      /* eslint quotes: [2, "single", "avoid-escape"] */
      fontFamily: "'Oswald', sans-serif",
      fontSize: '42px',
      fontWeight: 200,
      lineHeight: '45px',
      textAlign: 'left',
    },
  },
  sidePage: {
    background: 'rgb(17, 17, 17, 0.25)', // black, opacity 25%
    initialBackground: 'rgb(17, 17, 17, 0)',
    zIndex: 10000,
    color: colors.black,
    container: {
      background: colors.white,
      zIndex: 11,
    },
  },
  tmp: {
    test: 1,
  },

  dropDown: {
    border: `1px solid ${colors.silver}`,
    borderRadius: '4px',
    backgroundColor: 'transparent',
    color: `${colors.snow}`,
    minWidth: '100px',
    content: {},
    padding: '0.5rem 1rem',
    transition: transitions.defaultTransition,
    size: {
      large: {
        height: sizes.large.height,
        fontSize: sizes.large.fontSize,
        padding: '0rem 1rem',
        paddingArrow: '0 0 0 1rem',
        marginArrow: '0 0 0 0.5rem',
      },
      default: {
        height: sizes.default.height,
        fontSize: sizes.default.fontSize,
        padding: '0rem 0.8rem',
        paddingArrow: '0 0 0 0.7rem',
        marginArrow: '0 0 0 0.5rem',
      },
      small: {
        height: sizes.small.height,
        fontSize: sizes.small.fontSize,
        padding: '0rem 0.5rem',
        paddingArrow: '0 0 0 0.5rem',
        marginArrow: '0 0 0 0.5rem',
      },
    },
  },
  buttons: {
    size: {
      large: {
        height: sizes.large.height,
        fontSize: sizes.large.fontSize,
        padding: '0rem 1rem',
      },
      default: {
        height: sizes.default.height,
        fontSize: sizes.default.fontSize,
        padding: '0rem 0.8rem',
      },
      small: {
        height: sizes.small.height,
        fontSize: sizes.small.fontSize,
        padding: '0rem 0.5rem',
      },
    },
    primary: {
      border: `1px solid ${colors.main}`,
      borderRadius: '4px',
      backgroundColor: `${colors.main}`,
      color: `${colors.snow}`,
      padding: '0.5rem 1rem',
      transition: transitions.defaultTransition,
      minWidth: '60px',
      content: {},
      hover: {
        backgroundColor: `${colors.mainDark}`,
        color: `${colors.snow}`,
        borderColor: `${colors.mainDark}`,
      },
      active: {
        backgroundColor: `${colors.mainLight}`,
        color: `${colors.snow}`,
        borderColor: `${colors.mainLight}`,
      },
      disabled: {
        backgroundColor: `${colors.lightGrey}`,
        color: `${colors.darkGrey}`,
        borderColor: `${colors.lightGrey}`,
        cursor: 'default',
        opacity: 1,
      },
    },
    secondary: {
      border: `2px solid ${colors.main}`,
      borderRadius: '4px',
      backgroundColor: 'transparent',
      color: `${colors.main}`,
      padding: '0.5rem 1rem',
      transition: 'all linear .2s',
      minWidth: '60px',
      content: {},
      hover: {
        backgroundColor: 'transparent',
        color: `${colors.mainDark}`,
        borderColor: `${colors.mainDark}`,
      },
      active: {
        backgroundColor: 'transparent',
        color: `${colors.mainLight}`,
        borderColor: `${colors.mainLight}`,
      },
      disabled: {
        backgroundColor: 'transparent',
        color: `${colors.gray}`,
        borderColor: `${colors.gray}`,
        opacity: 0.5,
        cursor: 'default',
      },
    },
  },
};
