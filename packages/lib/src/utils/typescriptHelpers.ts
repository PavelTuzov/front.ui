import { RefObject } from 'react';

/**
 * Вспомогательный тип, который берет из U и T одинаковые элементы
 * Оригинальное описание из документации: Remove types from T that are not assignable to U
 */
export type Filter<T, U> = T extends U ? T : never;

/**
 * В текущей версии библиотеки типов реакта происходит коллизия в типе ref (пример):
 * Types of property 'ref' are incompatible.
 * Type 'string' is not assignable to type '((instance: HTMLInputElement | null) => void) | RefObject<HTMLInputElement> | null | undefined'.
 * Вкратце: ошибка в том, что в типе ref в @types/react указан дополнительно string.
 * При этом называется он LegacyRef, а судя по докам (https://reactjs.org/docs/refs-and-the-dom.html, Legacy API: String Refs):
 * We advise against it because string refs have some issues, are considered legacy, and are likely to be removed in one of the future releases.
 * Когда они его окончательно выпилят, это преобразование можно будет убрать
 */
export type LegacyRefHelper<T> =
  | ((instance: T | null) => void)
  | RefObject<T>
  | null
  | undefined;
