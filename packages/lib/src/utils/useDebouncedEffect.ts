import { useEffect } from 'react';

/**
 * Хук сокращения количества срабатываний
 * @param effect функция
 * @param delay задержка срабатываний (в ms)
 * @param deps массив зависимостей
 */
export const useDebouncedEffect = (
  effect: () => any,
  delay: number,
  deps: any[],
): void => {
  useEffect(() => {
    const handler = setTimeout(() => {
      effect();
    }, delay);
    return () => {
      clearTimeout(handler);
    };
  }, [effect, deps, delay]);
};
