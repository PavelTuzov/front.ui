/**
 * Склонение слова в зависимости от числа
 * @param {Number} number Число на основе которого нужно сформировать окончание
 * @param {Array} titles1 Массив слов (например ['символ', 'символа', 'символов'])
 * @param {Array} titles2 Массив слов (например ['Остался', 'Осталось', 'Осталось']) - необязательный параметр
 * @param {boolean} hideCount Флаг необходимости сокрытия числа при возврате результата (вернёт только склоняемое слово)
 * @return {String} пример "Осталось 12 символов"
 */
export const pluralizeAll = (
  number: number,
  titles1: string[],
  titles2?: string[],
  hideCount?: boolean,
): string => {
  const cases = [2, 0, 1, 1, 1, 2];

  const wordVariant: number =
    number % 100 > 4 && number % 100 < 20
      ? 2
      : cases[number % 10 < 5 ? number % 10 : 5];

  return `${titles2 ? `${titles2[wordVariant]} ` : ''}${
    !hideCount ? number : ''
  } ${titles1[wordVariant]}`;
};
