export interface ResizeHelperProps {
  addSubscription(handler: (e: Event) => any): any;
  removeSubscription(e: any): any;
}

export default function ResizeHelper(): ResizeHelperProps {
  let subscriptions: ((e: Event) => any)[] = [];

  /**
   * Обрабатывает изменение размера страницы
   * @param e
   */
  function onResize(e: Event): void {
    subscriptions.forEach(handler => handler(e));
  }

  window.addEventListener('resize', onResize);

  /** Добавляем в список подписок */
  function addSubscription(handler: (e: Event) => any): void {
    subscriptions.push(handler);
  }

  /** Убираем из списка подписок */
  function removeSubscription(handler: (e: Event) => any): void {
    subscriptions = subscriptions.filter(subs => subs !== handler);
  }

  return {
    addSubscription,
    removeSubscription,
  };
}
