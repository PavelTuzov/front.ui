import resolve from '@rollup/plugin-node-resolve';
import typescript from 'rollup-plugin-typescript2';
import commonjs from '@rollup/plugin-commonjs';
import babel from '@rollup/plugin-babel';
import progressbar from 'rollup-plugin-progressbar';

const prod = process.env.NODE_ENV === 'production';

const plugins = [
  resolve(),
  typescript({
    rollupCommonJSResolveHack: true,
  }),
  commonjs({
    include: /node_modules/,
  }),
  babel({
    exclude: 'node_modules/**',
    extensions: ['.js', '.ts', '.tsx'],
    babelHelpers: 'bundled',
  }),
  progressbar(),
];

export default {
  input: './src/index.tsx',
  output: [
    {
      file: './dist/bundle.cjs.js',
      format: 'cjs',
      sourcemap: !prod,
    },
    {
      file: './dist/bundle.js',
      format: 'es',
      sourcemap: !prod,
    },
  ],
  watch: {
    include: 'src/**',
  },
  plugins,
  external: [
    '@emotion/react',
    '@emotion/styled',
    'alphanum-sort',
    'emotion-theming',
    'imask',
    'nanoid',
    'react-imask',
    'react-remove-scroll',
    'react',
    'react-dom',
    'react/jsx-runtime',
    'react-transition-group',
    'react-responsive',
  ],
};
