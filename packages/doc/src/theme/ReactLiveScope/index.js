import React from 'react';
import * as Components from 'front.ui.lib';

const ReactLiveScope = {
  React,
  ...React,
  ...Components,
};

export default ReactLiveScope;