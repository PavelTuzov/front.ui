import React from 'react';
import MDXContent from '@theme-original/MDXContent';
import { ThemeProvider } from '@emotion/react';
import { defaultTheme } from 'front.ui.lib'

export default function MDXContentWrapper(props) {

  return (
    <>
      <ThemeProvider theme={defaultTheme}>
        <MDXContent {...props} />
      </ThemeProvider>
    </>
  );
}
